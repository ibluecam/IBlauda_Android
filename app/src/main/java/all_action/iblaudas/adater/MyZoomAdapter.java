package all_action.iblaudas.adater;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import all_action.iblaudas.*;
import all_action.iblaudas.app.AppController;
import all_action.iblaudas.model.Movie;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class MyZoomAdapter extends PagerAdapter {
	 ProgressBar spinner;
	Context context;
	ArrayList<HashMap<String, String>> viewImageData;
	LayoutInflater inflater;
	private List<Movie> movieItems;
	ImageLoader imageLoader = AppController.getInstance().getImageLoader();
	
	HashMap<String, String> resultp_slide = new HashMap<String, String>();
	public MyZoomAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylistImage) {
		this.context = context;
		viewImageData = arraylistImage;
		//imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		return viewImageData.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		//ImageView galeryImage;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.viewpager_item, container,false);
		resultp_slide = viewImageData.get(position);
		if (imageLoader == null)
			imageLoader = AppController.getInstance().getImageLoader();
		NetworkImageView thumbNail_slide = (NetworkImageView) row.findViewById(R.id.image_slide);
		
		thumbNail_slide.setImageUrl(resultp_slide.get("full_image_url"), imageLoader);
		TouchImageView img = new TouchImageView(context); 
		 img.setMaxZoom(4f);
		((ViewPager) container).addView(row);
		
		return row;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}