package all_action.iblaudas.adater;


import java.util.List;

import all_action.iblaudas.*;
import all_action.iblaudas.app.AppController;
import all_action.iblaudas.model.Movie;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class CustomListAdapter extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater;
	private List<Movie> movieItems;
    String mytitle,
           kindOrder,
           textnew;
    private String IconStatus,new_status;
	ImageLoader imageLoader = AppController.getInstance().getImageLoader();

	public CustomListAdapter(Activity activity, List<Movie> movieItems) {
		this.activity = activity;
		this.movieItems = movieItems;
	}

	@Override
	public int getCount() {
		return movieItems.size();
	}

	@Override
	public Object getItem(int location) {
		return movieItems.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView cano = (TextView) convertView.findViewById(R.id.Note);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView contry = (TextView) convertView.findViewById(R.id.country);
        TextView CarCity = (TextView) convertView.findViewById(R.id.genre);
        TextView txtfob = (TextView) convertView.findViewById(R.id.txtfob);
        TextView txtSaleOrReversed =(TextView)convertView.findViewById(R.id.txt_saleOrReversed);
        TextView txt_newstatus = (TextView)convertView.findViewById(R.id.txt_new_status);
        // getting movie data for the row
        Movie m = movieItems.get(position);
        //String Titles = m.getTitle().substring(m.getTitle().lastIndexOf(".....")+1);
        String mytitle = m.getTitle().substring(0,m.getTitle().length());
        // thumbnail image
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);
        Resources res = activity.getResources();
        //thumbNail.getLayoutParams().width=200;
        //thumbNail.getLayoutParams().height=200;
        thumbNail.getLayoutParams().width=res.getDimensionPixelSize(R.dimen.mysizeImage);
        thumbNail.getLayoutParams().height=res.getDimensionPixelSize(R.dimen.mysizeImage);
        txtSaleOrReversed.setTextSize(res.getDimensionPixelSize(R.dimen.title1));
        cano.setText(m.getNOT());
        title.setText(mytitle);
        contry.setText(m.getCountry());
        CarCity.setText(m.getCarCity());
        IconStatus = m.getStatusIcon();
        new_status = m.getCreate_status();
        if(IconStatus.equals("sale")){
            txtSaleOrReversed.setVisibility(View.INVISIBLE);
        }else{
            txtSaleOrReversed.getLayoutParams().width=res.getDimensionPixelSize(R.dimen.mysizeImage);
            txtSaleOrReversed.getLayoutParams().height=res.getDimensionPixelSize(R.dimen.mysizeImage);
            txtSaleOrReversed.setVisibility(View.VISIBLE);
            txtSaleOrReversed.setBackgroundResource(R.color.bg_reversed);
            txtSaleOrReversed.setText(m.getStatusIcon());
        }
        if(new_status.equals("new")){
            txt_newstatus.setVisibility(View.VISIBLE);
            txt_newstatus.getLayoutParams().width=res.getDimensionPixelSize(R.dimen.mysizeImage);
            txt_newstatus.getLayoutParams().height=res.getDimensionPixelSize(R.dimen.mysizeImage);
            txt_newstatus.setBackgroundResource(R.drawable.new_status);
        }else{
            txt_newstatus.setVisibility(View.INVISIBLE);
        }


        txtfob.setText(m.getFOB());


        return convertView;
    }
}