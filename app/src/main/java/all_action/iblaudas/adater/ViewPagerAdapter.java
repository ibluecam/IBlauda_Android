package all_action.iblaudas.adater;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import all_action.iblaudas.*;
import all_action.iblaudas.app.AppController;
import all_action.iblaudas.model.Movie;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class ViewPagerAdapter extends PagerAdapter {
	private ProgressBar spinner;
	Context context;
	ArrayList<HashMap<String, String>> viewImageData;
	LayoutInflater inflater;
	private List<Movie> movieItems;
	ImageLoader imageLoader = AppController.getInstance().getImageLoader();
	
	HashMap<String, String> resultp_slide = new HashMap<String, String>();
	public ViewPagerAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylistImage) {
		this.context = context;
		viewImageData = arraylistImage;
		//imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		return viewImageData.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		//ImageView galeryImage;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.viewpager_item, container,false);
		resultp_slide = viewImageData.get(position);
		if (imageLoader == null)
			imageLoader = AppController.getInstance().getImageLoader();
		spinner = (ProgressBar) row.findViewById(R.id.progressBar1);
		
		NetworkImageView thumbNail_slide = (NetworkImageView) row.findViewById(R.id.image_slide);
		
		thumbNail_slide.setImageUrl(resultp_slide.get("full_image_url"), imageLoader);

		Resources res = context.getResources();
		thumbNail_slide.getLayoutParams().width=res.getDimensionPixelSize(R.dimen.SizeImageSwip);
		thumbNail_slide.getLayoutParams().height=res.getDimensionPixelSize(R.dimen.SizeImageSwip);
		
		/*thumbNail_slide.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				Intent slideImage = new Intent(context,ZoomImage.class);
				slideImage.putExtra("idx", resultp_slide.get("idx"));
				context.startActivity(slideImage);
				
			}
		});*/
		
		//if(imageLoader!=null){
			
		//}
		((ViewPager) container).addView(row);

		//imageLoader.(resultp_slide.get("full_image_url"), thumbNail_slide, position,imageLoader);
		return row;
		
	}
	@Overrid
	public void onLoadingComplete() {
		spinner.setVisibility(View.GONE);
	}
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		 
		((ViewPager) container).removeView((RelativeLayout) object);
		
	}
}