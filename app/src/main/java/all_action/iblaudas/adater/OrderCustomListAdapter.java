package all_action.iblaudas.adater;


import java.util.List;

import all_action.iblaudas.*;
import all_action.iblaudas.app.AppController;
import all_action.iblaudas.model.Movie;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class OrderCustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Movie> movieItems;
    String member_no, remember_token,new_status;
    static final String DEFAULT ="";
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public OrderCustomListAdapter(Activity activity, List<Movie> movieItems) {
        this.activity = activity;
        this.movieItems = movieItems;
    }

    @Override
    public int getCount() {
        return movieItems.size();
    }

    @Override
    public Object getItem(int location) {
        return movieItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView cano = (TextView) convertView.findViewById(R.id.Note);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView contry = (TextView) convertView.findViewById(R.id.country);
        TextView CarCity = (TextView) convertView.findViewById(R.id.genre);
        TextView txtfob = (TextView) convertView.findViewById(R.id.txtfob);
        TextView txt_newstatus = (TextView)convertView.findViewById(R.id.txt_new_status);
        // getting movie data for the row
        final Movie m = movieItems.get(position);
        String mytitle = m.getTitle().substring(0,m.getTitle().length());
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);
        Resources res = activity.getResources();
        thumbNail.getLayoutParams().width=res.getDimensionPixelSize(R.dimen.mysizeImage);
        thumbNail.getLayoutParams().height=res.getDimensionPixelSize(R.dimen.mysizeImage);
        cano.setText(m.getNOT());
        title.setText(mytitle);
        contry.setText(m.getCountry());
        CarCity.setText(m.getCarCity());
        txtfob.setText(m.getFOB());
        txt_newstatus.setVisibility(View.INVISIBLE);

        return convertView;
	}

}