package all_action.iblaudas;


import ibaud.library.UserFunctions;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import all_action.iblaudas.adater.OrderCustomListAdapter;
import all_action.iblaudas.model.Movie;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ResourceAsColor") public class Orderlist extends Activity implements OnClickListener {
	private List<Movie> movieList = new ArrayList<Movie>();
	private ListView orderlistview;
	private OrderCustomListAdapter adapter;
	
	static String TAG_IMAGE = "image_name";
	static String TAG_IDEX = "idx";
	static String TAG_CAR_NO = "car_stock_no";
	static String TAG_YEAR = "car_year";
	static String TAG_MAKE = "car_make";
	static String TAG_MODEL = "car_model";
	static String TAG_COUNTRY = "country";
	static String TAG_FOB_COST = "car_fob_cost";
	static String TAG_CAR_CITY = "car_city";
	static String TAG_FOB_CURRENT = "car_fob_currency";
	static String TAG_NULL = "NULL";
	static final String DEFAULT ="";
	
	String member_no, remember_token;
	String IndeXID;
	int getPositionID;
	 
	private Button btnReserved;
	private Button btnHome;
	private Button btnchat;
	private Button btnlogin;
	private ImageView backimage;
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		bar.hide();
		setContentView(R.layout.order_list_car);
		FrameLayout header = (FrameLayout)findViewById(R.id.headerView2);
		getLayoutInflater().inflate(R.layout.header_orderlist, header);
		
		backimage = (ImageView)findViewById(R.id.btnBack);

		FrameLayout footer = (FrameLayout)findViewById(R.id.FootView);
		getLayoutInflater().inflate(R.layout.footer_all, footer);
		
		//===================================================
				RelativeLayout footerclick = (RelativeLayout)findViewById(R.id.footerclick);
				footerclick.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
					//Toast.makeText(getApplicationContext(), "Footer", 100).show();	
					}
				});
		btnHome = (Button)findViewById(R.id.btnHome);
		btnchat = (Button)findViewById(R.id.btnchat);
		btnlogin = (Button)findViewById(R.id.btnlogin);
		btnReserved = (Button)findViewById(R.id.btnreservedCar);
		btnReserved.setBackgroundResource(R.drawable.icon_mycar_selected);
		TextView home = (TextView)findViewById(R.id.textView2);
		home.setTextColor(R.color.myblue_text);
		btnHome.setOnClickListener(this);
		btnchat.setOnClickListener(this);
		btnlogin.setOnClickListener(this);
		backimage.setOnClickListener(this);
		btnReserved.setOnClickListener(this);
		
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		String member_id = user.getString("member_id", "");
		TextView logt = (TextView)findViewById(R.id.txt_log);
		home.setTextColor(R.color.myblue_text);
		if(member_id.equals("")){
			logt.setText(R.string.setlogin);
		}else{
			
			logt.setText(R.string.txt_account);
			}
		new NetCheck().execute();
	
		
	}
	
	/**
     * Async Task to check whether internet connection is working
     **/

    private class NetCheck extends AsyncTask<String,String,Boolean>
    {
        private ProgressDialog nDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            nDialog = new ProgressDialog(Orderlist.this);
            nDialog.setMessage("Loading..");
            nDialog.setTitle("Please wait...");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(false);
            nDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... args){
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(Boolean th){

            if(th == true){
                nDialog.dismiss();
                new ProcessRegister().execute();
            }
            else{
                nDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Error network connection", Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    private class ProcessRegister extends AsyncTask<String, String, JSONObject>  {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
    		member_no = user.getString("member_no", DEFAULT);
    		remember_token = user.getString("remember_token", DEFAULT);
 
        }

        @Override
        protected JSONObject doInBackground(String... args) {


            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.ReservedCar(member_no, remember_token );

            return json;


        }
        @SuppressLint("ShowToast") @Override
        protected void onPostExecute(JSONObject json) {
        	
        	try {
        		int myNum = 0;	
				JSONArray response = json.getJSONArray("cars");
				for (int i = 0; i < response.length(); i++) {
					try {

						JSONObject obj = response.getJSONObject(i);
						Movie movie = new Movie();
						
						movie.setIndex(obj.getString(TAG_IDEX));
						movie.setnote("NO: "+obj.getString(TAG_CAR_NO));
						movie.setTitle(obj.getString(TAG_MAKE)+" "+obj.getString(TAG_MODEL)+" "+obj.getString(TAG_YEAR));
						movie.setContry(obj.getString(TAG_COUNTRY));
						movie.setCarCity(obj.getString(TAG_CAR_CITY));
						String addfob = obj.getString(TAG_FOB_COST);
						if(addfob.equals("0")|| addfob.equals("null")){
							movie.setFOB("FOB: ASK");
						}else{
						
							myNum = Integer.parseInt(obj.getString(TAG_FOB_COST).toString());
							DecimalFormat df = new DecimalFormat("#,###");
							String result = df.format(myNum);
							movie.setFOB("FOB: "+result+" USD($)");
						}
						
						movie.setThumbnailUrl(obj.getString(TAG_IMAGE));
						movieList.add(movie);
						
						Log.e("Orderlist",""+obj.getString(TAG_CAR_NO));
						
					
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
					
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
           
        	if(movieList.size()==0){
				FrameLayout notresult = (FrameLayout)findViewById(R.id.content_view);
				getLayoutInflater().inflate(R.layout.noorderlist, notresult);
			}
        
        	orderlistview = (ListView) findViewById(R.id.listView_order_car);
			
			adapter = new OrderCustomListAdapter(Orderlist.this, movieList);
			orderlistview.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			orderlistview.setOnItemClickListener(new OnItemClickListener() {

				@SuppressLint("NewApi") @Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Movie m = movieList.get(position);
					getPositionID = position;
					 
					IndeXID=m.getIndex();
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(Orderlist.this);
					//ImageView title = new ImageView(getApplicationContext());
					//title.setImageResource(R.drawable.blauda_logo_dialog);
	
					//title.setGravity(Gravity.CENTER);
					//title.setPadding(0, 0, 0, 0);
					alertDialog.setTitle("IBlauda");
					
		            alertDialog.setCancelable(false);
		            // Setting Dialog Message
		            alertDialog.setMessage("Are you sure you want delete or see details this?");
		           // title.setBackgroundColor(R.color.bar_light_color);
		            // Setting Icon to Dialog
		          //  alertDialog.setIcon(R.drawable.blauda_logo);
		            // Setting Negative "NO" Button
		            alertDialog.setNegativeButton("Delete",
		                    new DialogInterface.OnClickListener() {
		                        public void onClick(DialogInterface dialog, int which) {
		                            // Write your code here to execute after dialog
		                        	new NetCheck1().execute();
		                        	//Toast.makeText(getApplicationContext(), IndeXID+"memberNo="+member_no+"toeken="+remember_token, 1000).show();
		                            dialog.cancel();
		                        }
		                    });
		            
		            	alertDialog.setNeutralButton("View Detail", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent todetail = new Intent(Orderlist.this,DetailCar.class);
							todetail.putExtra("IDX",IndeXID);
							startActivity(todetail);
							dialog.cancel();
							finish();
							
						}
					});
		            	// Setting Positive "Yes" Button
			            alertDialog.setPositiveButton("Cancel",
			                    new DialogInterface.OnClickListener() {
			                        public void onClick(DialogInterface dialog,int which) {
			                            // Write your code here to execute after dialog
			                        	dialog.cancel();
			                        }
			                    });
		           

		            // Showing Alert Message
			        alertDialog.show();
				
					//
					//Toast.makeText(getApplicationContext(), "MemeberNo="+member_no+"Token= "+remember_token+"Index="+m.getIndex(), Toast.LENGTH_LONG).show();
					
				}
			});
        }
//==================Cancel Order=================================
        /**
         * Async Task to check whether internet connection is working
         **/
        private class NetCheck1 extends AsyncTask<String,String,Boolean>
        {
            private ProgressDialog nDialog;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                nDialog = new ProgressDialog(Orderlist.this);
                nDialog.setMessage("Loading..");
                nDialog.setTitle("Checking Network");
                nDialog.setIndeterminate(false);
                nDialog.setCancelable(true);
                nDialog.show();
            }

            @Override
            protected Boolean doInBackground(String... args){
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnected()) {
                    try {
                        URL url = new URL("http://www.google.com");
                        HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                        urlc.setConnectTimeout(3000);
                        urlc.connect();
                        if (urlc.getResponseCode() == 200) {
                            return true;
                        }
                    } catch (MalformedURLException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return false;

            }
            @Override
            protected void onPostExecute(Boolean th){

                if(th == true){
                    nDialog.dismiss();
                    new ProcessCancelOrder().execute();
                }
                else{
                    nDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error network connection", Toast.LENGTH_SHORT).show();
                }
            }
        }
   
        private class ProcessCancelOrder extends AsyncTask<String, String, JSONObject> {

            @Override
            protected JSONObject doInBackground(String... args) {
              UserFunctions userFunction = new UserFunctions();
                JSONObject json_cancel = userFunction.CancelOrder(member_no, remember_token, IndeXID);
                return json_cancel;


            }
            
            @Override
            protected void onPostExecute(JSONObject json_cancel) {
            	            	String result;
            	String sucessMsg;
    			try {
    				result = json_cancel.getString("success");
    				sucessMsg = json_cancel.getString("msg");
    				if(result.equals("1")){
    					Toast.makeText(getApplicationContext(), sucessMsg, Toast.LENGTH_LONG).show();
    					 movieList.remove(getPositionID);
    					 adapter.notifyDataSetChanged();
    					//Toast.makeText(getApplicationContext(), ""+getPositionID, 100).show();
    				}else if(result.equals("0")){
    					Log.e("Json_CancelOrder_sucss0=", ""+result);
    					Log.e("Json_CancelOrder_msg0=", ""+sucessMsg);
    					//pDialog.dismiss();
    					Toast.makeText(getApplicationContext(), sucessMsg, Toast.LENGTH_LONG).show();
    				}
    				
    			} catch (JSONException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
            	
               
    			
            }
            
        }
		
    }

    
    public void ASettingAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		String member_id = user.getString("member_id", DEFAULT);
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent setting = new Intent(getApplicationContext(),AcountSettin.class);
			setting.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(setting);
			finish();	
		}
	} 
    public void loginAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		
		String member_id = user.getString("member_id", "");

		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent main = new Intent(getApplicationContext(), MainActivity.class);
			user.edit().clear().commit();
			startActivity(main);
		}
	}
	public void ChatAction(){
        if(member_no.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
        }else {
            Intent chatActivity = new Intent(this, ListChatUser.class);
            chatActivity.putExtra("MAIN", 1);
            startActivity(chatActivity);
            finish();
        }
	}
	public void MainAction(){
		Intent mainActivity = new Intent(this,MainActivity.class); 
		//mainActivity.putExtra("MAIN", main);
		startActivity(mainActivity);
		 finish();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnBack:
			MainAction();
			break;
	case R.id.btnHome:
		MainAction();
			break;
	case R.id.btnchat:
		ChatAction();
		break;
	case R.id.btnlogin:
		ASettingAction();
		break;
	case R.id.btnPerson:
		loginAction();
		break;

	case R.id.btnreservedCar:
		//ReservedAction();
		break;
		}
		
	}
}
