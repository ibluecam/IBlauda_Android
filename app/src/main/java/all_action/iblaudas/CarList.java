package all_action.iblaudas;


import com.android.volley.toolbox.JsonArrayRequest;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gcm.GCMRegistrar;

import all_action.iblaudas.adater.CustomListAdapter;
import all_action.iblaudas.app.AppController;
import all_action.iblaudas.model.Movie;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class CarList extends Activity implements OnClickListener{
	ListView listview;
	ProgressDialog mProgressDialog;
	
	static String TAG_IDEX = "idx";
	static String TAG_CAR_NO = "car_stock_no";
	static String TAG_YEAR = "car_year";
	static String TAG_MAKE = "car_make";
	static String TAG_MODEL = "car_model";
	static String TAG_COUNTRY = "country";
	static String TAG_FOB_COST = "car_fob_cost";
	static String TAG_CAR_CITY = "car_city";
    static String TAG_ICON_STATUS ="icon_status";
    static String TAG_CREATE_STATUS = "created_status";
    static String TAG_NEWDATE = "created_dt";
	static String TAG_FOB_CURRENT = "car_fob_currency";
	static String TAG_NULL = "NULL";
	String carlistbackview = "2";
	
	private List<Movie> movieList = new ArrayList<Movie>();
	private ListView listView;
	private CustomListAdapter adapter;
	
	public static final String DEFAULT ="";
	private Button btnReserved;
	private Button btnHome;
	private Button btnchat;
	private Button btnlogin;
	
	String main = "1";
	String carlistback = "2";
	String detialback = "3";
    SharedPreferences user;
    String member_id;
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	ActionBar bar = getActionBar();
	bar.hide();
	setContentView(R.layout.carlistview);
	Intent getsearch = getIntent();
	
	final String url = getsearch.getStringExtra("url_list");
	
	FrameLayout header = (FrameLayout)findViewById(R.id.headerView2);
	getLayoutInflater().inflate(R.layout.header_productlist, header);
	
	
	FrameLayout footer = (FrameLayout)findViewById(R.id.FootView);
	getLayoutInflater().inflate(R.layout.footer_all, footer);
	
	//===================================================
	RelativeLayout footerclick = (RelativeLayout)findViewById(R.id.footerclick);
	footerclick.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
				
		}
	});
	
	btnReserved = (Button)findViewById(R.id.btnreservedCar);
	btnHome = (Button)findViewById(R.id.btnHome);
	btnchat = (Button)findViewById(R.id.btnchat);
	btnlogin = (Button)findViewById(R.id.btnlogin);
	btnHome.setOnClickListener(this);
	btnchat.setOnClickListener(this);
	btnlogin.setOnClickListener(this);
	btnReserved.setOnClickListener(this);
	
	ImageView ImagBack = (ImageView)findViewById(R.id.btnBack);
	ImagBack.setOnClickListener(this);
	ImageView btnLogin = (ImageView)findViewById(R.id.btnPerson);
	btnLogin.setOnClickListener(this);
	
	final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
	member_id = user.getString("member_id", DEFAULT);
	TextView logt = (TextView)findViewById(R.id.txt_log);
	
	if(member_id.equals("")){
		logt.setText("Log in");
	}else{
		btnLogin.setImageResource(R.drawable.logout);
		logt.setText("Log out");
		}
	
		JsonArrayRequest movieReq = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						//Log.d(TAG, response.toString());
						int myNum = 0;				// Parsing json
						for (int i = 0; i < response.length(); i++) {
							try {

								JSONObject obj = response.getJSONObject(i);
								Movie movie = new Movie();
								movie.setIndex(obj.getString(TAG_IDEX));
								movie.setnote("NO: "+obj.getString(TAG_CAR_NO));
								movie.setTitle(obj.getString(TAG_MAKE)+" "+obj.getString(TAG_MODEL)+" "+obj.getString(TAG_YEAR));
								movie.setContry(obj.getString(TAG_COUNTRY));
								movie.setCarCity(obj.getString(TAG_CAR_CITY));
                                movie.setStatusIcon(obj.getString(TAG_ICON_STATUS));
                                movie.setCreateStatus(obj.getString(TAG_CREATE_STATUS));
								String addfob = obj.getString(TAG_FOB_COST);
								if(addfob.equals("0")|| addfob.equals("null")){
									movie.setFOB("FOB: ASK");
								}else{
									
									myNum = Integer.parseInt(obj.getString(TAG_FOB_COST).toString());
									DecimalFormat df = new DecimalFormat("#,###");
									String result = df.format(myNum);
									movie.setFOB("FOB: "+result+" USD($)");
								}
								movie.setThumbnailUrl(obj.getString("image_name"));
								movieList.add(movie);
								
								
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
						if(movieList.size()==0){
							FrameLayout notresult = (FrameLayout)findViewById(R.id.content_view);
							getLayoutInflater().inflate(R.layout.search_not_result, notresult);
						}
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						//VolleyLog.d(TAG, "Error: " + error.getMessage());
						
					}
				});
		
			listView = (ListView) findViewById(R.id.listView_feature);
			adapter = new CustomListAdapter(this, movieList);
			listView.setAdapter(adapter);
			AppController.getInstance().addToRequestQueue(movieReq);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long arg3) {
				String index_key = movieList.get(position).getIndex().toString();
				 String countryget = movieList.get(position).getCountry().toString();
	             Intent i = new Intent(getApplicationContext(), DetailCar.class);
	            //Toast.makeText(getApplicationContext(),url, 100).show();
	             i.putExtra("IDX", index_key);
	             i.putExtra("url_list", url);
	             i.putExtra(TAG_COUNTRY, countryget);
	             i.putExtra("ListCarBack", carlistbackview);
				startActivity(i);
				finish();
				
			}
		});
	//carList.setAdapter(new FeatureCarView(carImage));
	//new CarlistView().execute();
	}
	
	public void ASettingAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		String member_id = user.getString("member_id", DEFAULT);
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent setting = new Intent(getApplicationContext(),AcountSettin.class);
			setting.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(setting);
			finish();	
		}
	} 
	
	public void loginAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		String member_id = user.getString("member_id", DEFAULT);
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", main);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent main = new Intent(getApplicationContext(), MainActivity.class);
			user.edit().clear().commit();
			startActivity(main);
		}
	}
	public void ChatAction(){
        if(member_id.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", main);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        }else {
            Intent chatActivity = new Intent(getApplicationContext(), ListChatUser.class);
            chatActivity.putExtra("MAIN", main);
            chatActivity.putExtra("ListCarBack", carlistback);
            startActivity(chatActivity);
           // finish();
        }
	}
	public void MainAction(){
		Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class); 
		startActivity(mainActivity);
		 finish();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnHome:
				MainAction();
				break;
			case R.id.btnchat:
				ChatAction();
				break;
			case R.id.btnlogin:
				ASettingAction();
				break;
			case R.id.btnPerson:
				loginAction();
				break;
			case R.id.btnreservedCar:
				final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
				String member_id = user.getString("member_id", DEFAULT);
				if(member_id.equals("")){
					Intent login = new Intent(getApplicationContext(),Login.class);
					login.putExtra("ListCarBack", carlistback);
					login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(login);
					finish();
					//Toast.makeText(getApplicationContext(), "Logo", 100).show();
				}else{
					Intent orderlist = new Intent(getApplicationContext(),Orderlist.class);
					startActivity(orderlist);
					finish();
					//Toast.makeText(getApplicationContext(), "sdfsd", 100).show();
					//NetAsync(v);
				}
				break;
			default:
				Intent back = new Intent(this,MainActivity.class);
				startActivity(back);
				finish();
				break;
			}
		
		//}
	}
	
	/**
	 * Receiving push messages
	 **/
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//String newMessage = intent.getExtras().getString("");
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());
			WakeLocker.release();
		}
	};

	@Override
	protected void onDestroy() {
		
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}
	   
}
	 
