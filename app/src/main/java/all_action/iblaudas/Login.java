package all_action.iblaudas;


import ibaud.library.UserFunctions;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Login extends Activity implements OnClickListener {
    private Button Loginbtn;
    private ImageView btnback;
    private TextView register_now;
    private EditText txt_userID, txt_pwd;
    private TextView mErrorField;
    /**
     * Called when the activity is first created.
     */
    private static String KEY_SUCCESS = "success";
    public static final String DEFAULT ="";
    Button btnReserved;
    private Button btnHome;
    private Button btnchat;
    private Button btnlogin;
    String main = "1";
    String carlistback = "2";
    String detialback = "3";
    SharedPreferences user;
   String member_no;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar bar = getActionBar();
        bar.hide();
        setContentView(R.layout.home_login);

        FrameLayout header = (FrameLayout)findViewById(R.id.headerView);
        getLayoutInflater().inflate(R.layout.header_home, header);

        FrameLayout content = (FrameLayout)findViewById(R.id.contentHome);
        getLayoutInflater().inflate(R.layout.content_home, content);
        FrameLayout footer = (FrameLayout)findViewById(R.id.FootView);
        getLayoutInflater().inflate(R.layout.footer_all, footer);
        btnReserved = (Button)findViewById(R.id.btnreservedCar);
        btnHome = (Button)findViewById(R.id.btnHome);
        btnchat = (Button)findViewById(R.id.btnchat);
        btnlogin = (Button)findViewById(R.id.btnlogin);
        btnHome.setOnClickListener(this);
        btnchat.setOnClickListener(this);
        btnlogin.setOnClickListener(this);

        TextView viewt = (TextView)findViewById(R.id.don_have_account);
        viewt.setText("Don't have account?");
        txt_userID = (EditText)findViewById(R.id.txt_userID);
        txt_pwd = (EditText)findViewById(R.id.txt_pwd);
        Loginbtn = (Button)findViewById(R.id.btn_login);
        btnback = (ImageView)findViewById(R.id.btnBack);
        register_now = (TextView)findViewById(R.id.register_now);
        register_now.setOnClickListener(this);
        btnback.setOnClickListener(this);
        Loginbtn.setOnClickListener(this);

        btnlogin.setBackgroundResource(R.drawable.login_person_selected);
         user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
         member_no = user.getString("member_id", "");
        TextView logt = (TextView)findViewById(R.id.txt_log);

        if(member_no.equals("")){
            logt.setText(R.string.setlogin);
        }else{
            logt.setText(R.string.txt_account);

        }
    }

    public void ChatAction(){
        if(member_no!=""){
            Intent chatActivity = new Intent(getApplicationContext(), ListChatUser.class);
            chatActivity.putExtra("MAIN", main);
            startActivity(chatActivity);
            finish();
        }else {
            Toast.makeText(getApplicationContext(),"Please Login.",Toast.LENGTH_LONG).show();
        }
    }
    public void MainAction(){
        Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
        //mainActivity.putExtra("MAIN", main);
        startActivity(mainActivity);
        finish();
    }
    @Override
    public void onClick(final View v) {
        Bundle extras = getIntent().getExtras();
        String value_back = extras.getString("MAIN");
        String value_back1 = extras.getString("ListCarBack");
        String value_back2 = extras.getString("cardetail");
        String urlsearch = extras.getString("url_list");
        String idx = extras.getString("IDX");
        switch(v.getId()){
            case R.id.btn_login:

                txt_userID.setError(null);
                txt_pwd.setError(null);
                if (  ( !txt_userID.getText().toString().equals("")) && ( !txt_pwd.getText().toString().equals("")) )
                {
                    NetAsync(v);
                }
                else if ( ( !txt_userID.getText().toString().equals("")) )
                {
                    txt_pwd.setError("User ID is empty");

                }
                else if ( ( !txt_pwd.getText().toString().equals("")) )
                {
                    txt_userID.setError("Password is empty");
                }
                else
                {
                    txt_userID.setError("User ID is empty");
                    txt_pwd.setError("Password is empty");
                }
                break;
            case R.id.btnBack:
                if (extras != null) {

                    if(value_back!=null ){
                        Intent backMain = new Intent(Login.this,MainActivity.class);
                        backMain.putExtra("MAIN", main);
                        //Toast.makeText(getApplicationContext(), value_back, 100).show();
                        startActivity(backMain);
                        finish();
                    }else if(value_back1!=null){
                        Intent backlistcar = new Intent(Login.this,CarList.class);
                        backlistcar.putExtra("url_list", urlsearch);
                        backlistcar.putExtra(value_back1, value_back1);
                        startActivity(backlistcar);
                        finish();
                    } else if(value_back2!=null){
                        Intent backcarDetail = new Intent(Login.this,DetailCar.class);
                        backcarDetail.putExtra("IDX", idx);
                        backcarDetail.putExtra(value_back2, value_back2);
                        startActivity(backcarDetail);
                        finish();
                    }else{
                        Intent backMain = new Intent(Login.this,MainActivity.class);
                        backMain.putExtra("MAIN", main);
                        startActivity(backMain);
                        finish();
                    }
                }

                break;
            case R.id.register_now:
                if (extras != null) {
                    Intent register = new Intent(Login.this,Register.class);
                    register.putExtra("url_list", urlsearch);
                    register.putExtra("IDX", idx);
                    register.putExtra("MAIN", value_back);
                    register.putExtra("ListCarBack", value_back1);
                    register.putExtra("cardetail", value_back2);
                    //Toast.makeText(getApplicationContext(), idx+","+urlsearch+","+value_back1+","+value_back2+""+value_back, 100).show();
                    startActivity(register);
                    finish();
                }
                break;
            case R.id.btnHome:
                MainAction();
                break;
            case R.id.btnreservedCar:
                //Toast.makeText(getApplicationContext(), "Please login....", 150).show();
                break;
            case R.id.btnchat:
                ChatAction();
                break;

        }
    }

    private class NetCheck extends AsyncTask<String,String,Boolean>
    {
        private ProgressDialog nDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            nDialog = new ProgressDialog(Login.this);
            nDialog.setTitle("Checking Network");
            nDialog.setMessage("Loading..");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(false);
            nDialog.show();
        }
        /**
         * Gets current device state and checks for working internet connection by trying Google.
         **/
        @Override
        protected Boolean doInBackground(String... args){
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(Boolean th){

            if(th == true){
                nDialog.dismiss();
                new ProcessLogin().execute();
            }
            else{
                nDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Error network connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class ProcessLogin extends AsyncTask<String, String, JSONObject> {


        private ProgressDialog pDialog;

        String email,password;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            email = txt_userID.getText().toString();
            password = txt_pwd.getText().toString();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setTitle("Contacting Servers");
            pDialog.setMessage("Logging in ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            UserFunctions userFunction = new UserFunctions();

            JSONObject json = userFunction.loginUser(email, password);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {

            try {
                if (json.getString(KEY_SUCCESS) != null) {

                    String res = json.getString(KEY_SUCCESS);
                    Log.d("key: ", "> " + res);

                    if(Integer.parseInt(res) == 1){
                        pDialog.setMessage("Login success ...");
                        pDialog.setTitle("Welcome");
                        //get user info
                        JSONObject json_user = json.getJSONObject("user");
                        //put in SharedPreferences
                        SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = user.edit();
                        editor.putString("member_id", json_user.getString("member_id"));
                        editor.putString("member_first_name", json_user.getString("member_first_name"));
                        editor.putString("member_last_name", json_user.getString("member_last_name"));
                        editor.putString("email", json_user.getString("email"));
                        editor.putString("member_country", json_user.getString("member_country"));
                        editor.putString("member_no", json_user.getString("member_no"));
                        editor.putString("remember_token", json_user.getString("remember_token"));
                        editor.commit();

                        Intent upanel = new Intent(getApplicationContext(), MainActivity.class);
                        //upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pDialog.dismiss();
                        startActivity(upanel);
                        finish();
                    }
                    else{
                        pDialog.dismiss();
                        Toast.makeText(Login.this , "Incorrect username/password", Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public void NetAsync(View view){
        new NetCheck().execute();
    }

    public void onPause()
    {
        super.onPause();
        // Log.d(tag, "In the onPause() event");
    }
    public void onStop()
    {
        super.onStop();
        // Log.d(tag, "In the onStop() event");
    }





}

