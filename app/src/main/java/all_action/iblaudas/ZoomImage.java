package all_action.iblaudas;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import all_action.iblaudas.adater.MyZoomAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.ImageView;

public class ZoomImage extends Activity {
	  ImageView img;
	  Bitmap bitmap;
	  ProgressDialog pDialog;
	public ZoomImage() {
		// TODO Auto-generated constructor stub
	}
	ArrayList<HashMap<String, String>> arraylistImage;
	ProgressDialog mProgressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	
	ActionBar bar = getActionBar();
	bar.hide();
	setContentView(R.layout.lastscreen);
	

	//Toast.makeText(getApplicationContext(), imageShow, 100).show();
	//TouchImageView img = new TouchImageView(this);  
   // img.setImageResource(R.drawable.icon_home_selected); 
	//img.equals(imageShow);
   // img.setMaxZoom(4f);  
   // setContentView(img);
	//load_img = (Button)findViewById(R.id.load);
    // img = (ImageView)findViewById(R.id.img);
   // load_img.setOnClickListener(new View.OnClickListener() {
    	
     // @Override
     // public void onClick(View arg0) {
        // TODO Auto-generated method stub
        // new LoadImage().execute(imageShow);
     // }
   // });
	 new LoadImage().execute();
  }
	
private class LoadImage extends AsyncTask<Void, Void, Void>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(ZoomImage.this);
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(true);
			// Show progressdialog
			//mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Intent getImage = getIntent();
	    	String imageShow = getImage.getStringExtra("idx");
			String urlImage = "http://iblauda.com/?c=json&m=getCarImages&idx="+imageShow;
			ServiceHandler sh = new ServiceHandler();
			String jsonStr = sh.makeServiceCall(urlImage, ServiceHandler.GET);
	
		//Log.d("Response: ", "> " + jsonStr);
			if (jsonStr != null) {
				try {
					

					JSON_buffer imageCar = new JSON_buffer();
					JSONArray dataas = new JSONArray(imageCar.getJSONUrl(urlImage));
					arraylistImage = new ArrayList<HashMap<String, String>>();
					HashMap<String,String> listImage;
					// looping through All Contacts
					for (int s = 0; s < dataas.length(); s++) {
						JSONObject d = dataas.getJSONObject(s);
						listImage = new HashMap<String, String>();
						listImage.put("full_image_url", d.getString("full_image_url"));
						arraylistImage.add(listImage);
						Log.e("Image", ""+arraylistImage);
					}	
					
					
				} catch (JSONException e) {
					e.printStackTrace();
				} 
			} else {
				//Log.e("ServiceHandler", "Couldn't get any data from the url");
			}
			return null;	
		}

		@Override
		protected void onPostExecute(Void args) {
			ViewPager viewPager = (ViewPager) findViewById(R.id.pagerView);
		     // Pass results to ViewPagerAdapter Class
			MyZoomAdapter adaptera = new MyZoomAdapter(ZoomImage.this,arraylistImage);
			viewPager.setAdapter(adaptera);
			 
			  
				
	}	
	
}}	
	
 /* private class LoadImage extends AsyncTask<String, String, Bitmap> {
    @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ZoomImage.this);
            pDialog.setMessage("Loading Image ....");
            pDialog.show();
    }
       protected Bitmap doInBackground(String... args) {
         try {
               bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
        } catch (Exception e) {
              e.printStackTrace();
        }
      return bitmap;
       }
       protected void onPostExecute(Bitmap image) {
         if(image != null){
        	TouchImageView img = new TouchImageView(getApplicationContext()); 
        	img.setMaxZoom(4f);  
        	img.setMaxZoom(4f);  
            img.setImageBitmap(image);
            setContentView(img);
           pDialog.dismiss();
         }else{
           pDialog.dismiss();
           Toast.makeText(ZoomImage.this, "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();
         }
       }*/
   
