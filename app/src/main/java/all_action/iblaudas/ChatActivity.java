package all_action.iblaudas;

import java.util.ArrayList;

import TaskModelAndAdapter.Task;
import TaskModelAndAdapter.TaskAdapter;

import all_action.iblaudas.adater.ChatListAdapter;
import all_action.iblaudas.model.Message;
import ibaud.library.UserFunctions;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({ "NewApi", "ResourceAsColor" })
public class ChatActivity extends Activity implements OnClickListener, OnItemClickListener {
    MediaPlayer mp = null;
    private EditText mTaskInput;
    private ListView mListView;
    private TaskAdapter mAdapter;
    private Button btnReserved;
    private Button btnHome;
    private Button btnchat;
    private Button btnlogin;
    private Button submit_button;
    String receiver_no;
    String userName;
    String memeber_no;
    String remember_token;
    String lastCM = "";
    String cm_id;
    String msg;
    ArrayList<Message> listMessage;
    ArrayList<Message> CM;
    private ChatListAdapter adapter;
    ListView lvChat;
    public static final String DEFAULT ="";
    private Handler handler = new Handler();
    String userID1,UserID2;
    String cmID,last_user,user2,users;
    int result;
    ConnectionDetector cd;
    @SuppressLint({ "NewApi", "SetJavaScriptEnabled" }) @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar bar = getActionBar();
        bar.hide();
        setContentView(R.layout.activity_chat);

        FrameLayout header = (FrameLayout)findViewById(R.id.header_chat_View);
        getLayoutInflater().inflate(R.layout.header_chat, header);

        FrameLayout footer = (FrameLayout)findViewById(R.id.FootView);
        getLayoutInflater().inflate(R.layout.footer_all, footer);

        final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        TextView logt = (TextView)findViewById(R.id.txt_log);
        TextView home = (TextView)findViewById(R.id.textView1);
        home.setTextColor(R.color.myblue_text);
        if(member_id.equals("")){
            logt.setText(R.string.setlogin);
        }else{

            logt.setText(R.string.txt_account);
        }

        mAdapter = new TaskAdapter(this, new ArrayList<Task>());


        Intent test = getIntent();
        receiver_no = test.getStringExtra("receiver_no");
        userName = test.getStringExtra("user");
        TextView chat = (TextView)findViewById(R.id.tvChat);
        chat.setText(userName);

        //handler.postDelayed(runnable, 100);
        new getMessage().execute();
        // Run the runnable object defined every 100ms



        btnReserved = (Button)findViewById(R.id.btnreservedCar);
        btnHome = (Button)findViewById(R.id.btnHome);
        btnchat = (Button)findViewById(R.id.btnchat);
        btnlogin = (Button)findViewById(R.id.btnlogin);
        btnHome.setOnClickListener(this);
        btnchat.setOnClickListener(this);
        btnlogin.setOnClickListener(this);
        btnReserved.setOnClickListener(this);
        btnchat.setBackgroundResource(R.drawable.icon_chat_selected);

        ImageView btnback = (ImageView)findViewById(R.id.btnBack);
        btnback.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                String idx = extras.getString("IDX");

                String main1 = extras.getString("MAIN");
                String detailA = extras.getString("cardetail");
                String paglistchat = extras.getString("pagelistchat");
                String listchat = extras.getString("listchat");
                if(main1!=null){
                    Intent main = new Intent(ChatActivity.this, MainActivity.class);
                    startActivity(main);
                    finish();
                }else if(detailA!=null){
                    Intent back = new Intent(ChatActivity.this, DetailCar.class);
                    back.putExtra("IDX", idx);
                    startActivity(back);
                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                }else if (paglistchat!=null){
                    Intent backtolist = new Intent(ChatActivity.this, ListChatUser.class);
                    backtolist.putExtra("pagelistchat", paglistchat);
                    startActivity(backtolist);
                    backtolist.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                }else if(listchat!=null){
                    Intent backtolistchat = new Intent(ChatActivity.this, ListChatUser.class);
                    backtolistchat.putExtra("pagelistchat", paglistchat);
                    startActivity(backtolistchat);
                    backtolistchat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                }else{
                    Intent main = new Intent(ChatActivity.this, MainActivity.class);
                    startActivity(main);
                    finish();
                }
            }
        });



        submit_button = (Button)findViewById(R.id.submit_button);
        submit_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mTaskInput = (EditText)findViewById(R.id.task_input);
                msg = mTaskInput.getText().toString();
                if(msg.length()>0) {
                    cd = new ConnectionDetector(getApplicationContext());
                    // Check if Internet present
                    if (!cd.isConnectingToInternet()) {
                        // Internet Connection is not present
                        Toast.makeText(getApplicationContext(),"Please check the Internet...",Toast.LENGTH_LONG).show();
                    } else {
                        new setupMessagePosting().execute();
                        mTaskInput.setText("");
                    }
                }else{

                    // mp.stop();
                    //Toast.makeText(getApplicationContext(),"Write something..",Toast.LENGTH_LONG).show();
                }
            }
        });
        handler.postDelayed(runnable, 1000);
    }



    // Defines a runnable which is run every 100ms
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

                refreshMessages();
                handler.postDelayed(this, 1000);

        }
    };

    private void refreshMessages() {
        new getLastCM().execute();
       // new getNewMsgs().execute();
        if(!lastCM.equals(cm_id)){
            new getMessage().execute();

        }
        //Toast.makeText(ChatActivity.this, lastCM, Toast.LENGTH_SHORT).show();
    }



    public void ASettingAction(){
        final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        if(member_id.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        }else{
            Intent setting = new Intent(getApplicationContext(),AcountSettin.class);
            setting.putExtra("MAIN", 1);
            //Toast.makeText(getApplicationContext(), main, 100).show();
            setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(setting);
            finish();
        }
    }

    public void loginAction(){
        final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        if(member_id.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        }else{
            Intent main = new Intent(getApplicationContext(), MainActivity.class);
            user.edit().clear().commit();
            startActivity(main);
        }
    }
    public void ChatAction(){
        if(memeber_no.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        }else {
           /* Intent chatActivity = new Intent(getApplicationContext(), ListChatUser.class);
            chatActivity.putExtra("MAIN", 1);
            startActivity(chatActivity);
            finish();*/
        }
    }
    public void MainAction(){
        Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(mainActivity);
        handler.removeCallbacksAndMessages( ChatActivity.class );
        finish();
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnHome:
                MainAction();
                break;
            case R.id.btnchat:
                ChatAction();
                break;
            case R.id.btnlogin:
                ASettingAction();
                break;
            case R.id.btnPerson:
                loginAction();
                break;
            case R.id.btnreservedCar:
                final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                String member_id = user.getString("member_id", "");
                if(member_id.equals("")){
                    Intent login = new Intent(getApplicationContext(),Login.class);
                    login.putExtra("ListCarBack", "2");
                    login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(login);
                    finish();
                    //Toast.makeText(getApplicationContext(), "Logo", 100).show();
                }else{
                    Intent orderlist = new Intent(getApplicationContext(),Orderlist.class);
                    startActivity(orderlist);
                    finish();
                    //Toast.makeText(getApplicationContext(), "sdfsd", 100).show();
                    //NetAsync(v);
                }
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Task task = mAdapter.getItem(position);
        TextView taskDescription = (TextView) view.findViewById(R.id.task_description);

        task.setCompleted(!task.isCompleted());

        if(task.isCompleted()){
            taskDescription.setPaintFlags(taskDescription.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            taskDescription.setPaintFlags(taskDescription.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

        task.saveEventually();

    }

    private class getMessage extends AsyncTask<String, String, JSONObject> {

        /**
         * Defining Process dialog
         */
        private ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            UserFunctions userFunction = new UserFunctions();
            SharedPreferences chatSetting = getSharedPreferences("chatSetting", Context.MODE_PRIVATE);
            memeber_no = chatSetting.getString("memeber_no",DEFAULT);
            remember_token = chatSetting.getString("remember_token",DEFAULT);
            receiver_no = chatSetting.getString("receiver_no",DEFAULT);
            Log.e("memeber_no",memeber_no);
            Log.e("remember_token",remember_token);
            Log.e("receiver_no",receiver_no);

            JSONObject json = userFunction.ReceiveMessage(memeber_no, remember_token, receiver_no);
            return json;

        }

        @Override
        protected void onPostExecute(JSONObject json) {

            /**
             * Checks for success message.
             **/
            try {
                String res = json.getString("success");
                Log.d("key suc: ", "> " + res);

                JSONArray jsonarray = json.getJSONArray("conversation_msgs");
                listMessage = new ArrayList<Message>();
                Log.d("leaght" , String.valueOf(jsonarray.length()));
                for (int i = 0; i < jsonarray.length(); i++){
                    JSONObject jsonobject  = jsonarray.getJSONObject(i);

                    Message mMessage = new Message();
                    mMessage.setText(jsonobject.getString("conversation_text"));
                    mMessage.setMessageId(jsonobject.getString("member_name"));
                    mMessage.setSenderId(jsonobject.getString("author"));
                    mMessage.setRecipientId(jsonobject.getString("user2"));
                    users = jsonobject.getString("author");
                    mMessage.setCm_id(jsonobject.getString("cm_id"));
                    String createDate = (jsonobject.getString("created_dt"));

                    mMessage.setCreated_dt(createDate.substring(10,createDate.length()));
                    listMessage.add(mMessage);
                    lvChat = (ListView) findViewById(R.id.lvChat);
                    adapter = new ChatListAdapter(ChatActivity.this, memeber_no, listMessage);
                    adapter.notifyDataSetChanged();
                    lvChat.setAdapter(adapter);
                    lvChat.invalidateViews();
                    lastCM = listMessage.get(listMessage.size()-1).getCm_id();


                }
              /*  mp = MediaPlayer.create(getApplicationContext(), R.raw.mysound);
                Log.e("users=",users);
                Log.e("remsdf=",receiver_no);
                if(!users.equals(memeber_no)) {;
                    mp.start();

                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    //========================Check internet post msg========================

    private class setupMessagePosting extends AsyncTask<String, String, JSONObject> {

        /**
         * Defining Process dialog
         */
        private ProgressDialog pDialog;



        @Override
        protected JSONObject doInBackground(String... args) {
            UserFunctions userFunction = new UserFunctions();
            //mTaskInput = (EditText)findViewById(R.id.task_input);
           // msg = mTaskInput.getText().toString();

                SharedPreferences chatSetting = getSharedPreferences("chatSetting", Context.MODE_PRIVATE);
                memeber_no = chatSetting.getString("memeber_no", DEFAULT);
                remember_token = chatSetting.getString("remember_token", DEFAULT);
                receiver_no = chatSetting.getString("receiver_no", DEFAULT);
                JSONObject json = userFunction.setupMessagePosting(memeber_no, remember_token, receiver_no, msg);

                return json;

        }

        @Override
        protected void onPostExecute(JSONObject json) {

            /**
             * Checks for success message.
             **/
            try {
                String res = json.getString("success");
                Log.d("key suc: ", "> " + res);
                Message mMessage = new Message();
                mMessage.setText(msg);
                mMessage.setSenderId(memeber_no);
                mMessage.setRecipientId(receiver_no);
                mMessage.setCm_id(lastCM);
                /*listMessage.add(mMessage);
                lvChat = (ListView) findViewById(R.id.lvChat);
                adapter = new ChatListAdapter(ChatActivity.this, memeber_no, listMessage);
                adapter.notifyDataSetChanged();
                lvChat.setAdapter(adapter);
                lvChat.invalidateViews();*/
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private class getLastCM extends AsyncTask<String, String, JSONObject> {

        /**
         * Defining Process dialog
         */
        private ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            UserFunctions userFunction = new UserFunctions();
            SharedPreferences chatSetting = getSharedPreferences("chatSetting", Context.MODE_PRIVATE);
            memeber_no = chatSetting.getString("memeber_no",DEFAULT);
            remember_token = chatSetting.getString("remember_token",DEFAULT);
            receiver_no = chatSetting.getString("receiver_no",DEFAULT);

            JSONObject json = userFunction.ReceiveMessage(memeber_no, remember_token,receiver_no);
            return json;

        }

        @Override
        protected void onPostExecute(JSONObject json) {

            SharedPreferences getmewMsg = getSharedPreferences("getNewMsg", Context.MODE_PRIVATE);
            cmID = getmewMsg.getString("last_cmd", "");
            last_user = getmewMsg.getString("last_user", "");
             user2 = getmewMsg.getString("last_user2","");
            /**
             * Checks for success message.
             **/
            try {
                String res = json.getString("success");
                Log.d("key suc: ", "> " + res);

                JSONArray jsonarray = json.getJSONArray("conversation_msgs");
                CM = new ArrayList<Message>();
                Log.d("leaght" , String.valueOf(jsonarray.length()));
                for (int i = 0; i < jsonarray.length(); i++){
                    JSONObject jsonobject  = jsonarray.getJSONObject(i);

                    Message mMessage = new Message();
                    mMessage.setCm_id(jsonobject.getString("cm_id"));
                    mMessage.setUserID(jsonobject.getString("user2"));
                    CM.add(mMessage);
                    cm_id = CM.get(CM.size()-1).getCm_id();

                }


                //Log.e("LasCmID=",cm_id);
               // Log.e("LasstUerID=",userID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private String tag="Chat Activity";
    public void onStart()
    {
        super.onStart();
        Log.d(tag, "In the onStart() event");
    }
    public void onRestart()
    {
        super.onRestart();
        handler.postDelayed(runnable, 1000);
        Log.d(tag, "In the onRestart() event");
    }
    public void onResume()
    {
        super.onResume();
        Log.d(tag, "In the onResume() event");
    }
    public void onPause()
    {
        super.onPause();
        //this.finish();
        Log.d(tag, "In the onPause() event");
    }
    public void onStop()
    {
        super.onStop();
        handler.removeCallbacks(runnable);
        Log.d(tag, "In the onStop() event");
    }
    @Override
    protected void onDestroy() {

        super.onDestroy();
       handler.removeCallbacks(runnable);
    }
}
