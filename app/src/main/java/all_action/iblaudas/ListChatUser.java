package all_action.iblaudas;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import TaskModelAndAdapter.TaskAdapter;
import all_action.iblaudas.adater.ListUserAdapter;
import all_action.iblaudas.model.User;
import ibaud.library.UserFunctions;

@SuppressLint({ "NewApi", "ResourceAsColor" })
public class ListChatUser extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private EditText mTaskInput;
    private ListView mListView;
    private TaskAdapter mAdapter;

    private Button btnReserved;
    private Button btnHome;
    private Button btnchat;
    private Button btnlogin;

    private List<User> listUser;
    private ListView listView;
    private ListUserAdapter adapter;
    //ArrayList<String> list;
    //private static String url = "http://192.168.0.210/mobile/getUserList.php";
    SharedPreferences user;
    String memeber_no;
    String cmID, last_user;
    private Handler handler = new Handler();

    @SuppressLint({"NewApi", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar bar = getActionBar();
        bar.hide();
        setContentView(R.layout.activity_list_chat_user);

        FrameLayout header = (FrameLayout) findViewById(R.id.header_chat_View);
        getLayoutInflater().inflate(R.layout.header_chat, header);

        FrameLayout footer = (FrameLayout) findViewById(R.id.FootView);
        getLayoutInflater().inflate(R.layout.footer_all, footer);

        user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        memeber_no = user.getString("memeber_no", "");
        TextView logt = (TextView) findViewById(R.id.txt_log);
        TextView home = (TextView) findViewById(R.id.textView1);
        home.setTextColor(R.color.myblue_text);
        if (member_id.equals("")) {
            logt.setText(R.string.setlogin);
        } else {

            logt.setText(R.string.txt_account);
        }
        new getUserList().execute();
        handler.postDelayed(runnable, 20000);
        SharedPreferences getmewMsg = getSharedPreferences("getNewMsg", Context.MODE_PRIVATE);
        cmID = getmewMsg.getString("last_cmd", "");
        last_user = getmewMsg.getString("last_user", "");
        String user2 = getmewMsg.getString("last_user2", "");

        Log.e("fcmd", cmID);
        Log.e("user1", last_user);
        Log.e("user2", user2);

        btnReserved = (Button) findViewById(R.id.btnreservedCar);
        btnHome = (Button) findViewById(R.id.btnHome);
        btnchat = (Button) findViewById(R.id.btnchat);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        btnHome.setOnClickListener(this);
        btnchat.setOnClickListener(this);
        btnlogin.setOnClickListener(this);
        btnReserved.setOnClickListener(this);
        btnchat.setBackgroundResource(R.drawable.icon_chat_selected);

        ImageView btnback = (ImageView) findViewById(R.id.btnBack);
        btnback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                String idx = extras.getString("IDX");

                String main1 = extras.getString("MAIN");
                String detailA = extras.getString("cardetail");
                String paglistchat = extras.getString("pagelistchat");

                if (main1 != null) {
                    Intent main = new Intent(ListChatUser.this, MainActivity.class);
                    startActivity(main);
                    finish();
                } else if (detailA != null) {
                    Intent back = new Intent(ListChatUser.this, DetailCar.class);
                    back.putExtra("IDX", idx);
                    startActivity(back);
                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                } else if (paglistchat != null) {
                    Intent backtolist = new Intent(ListChatUser.this, ListChatUser.class);
                    backtolist.putExtra("pagelistchat", paglistchat);
                    startActivity(backtolist);
                    backtolist.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                } else {
                    Intent main = new Intent(ListChatUser.this, MainActivity.class);
                    startActivity(main);
                    finish();
                }
            }
        });

    }

    public void ASettingAction() {
        final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        if (member_id.equals("")) {
            Intent login = new Intent(getApplicationContext(), Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        } else {
            Intent setting = new Intent(getApplicationContext(), AcountSettin.class);
            setting.putExtra("MAIN", 1);
            //Toast.makeText(getApplicationContext(), main, 100).show();
            setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(setting);
            finish();
        }
    }

    public void loginAction() {
        final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        if (member_id.equals("")) {
            Intent login = new Intent(getApplicationContext(), Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        } else {
            Intent main = new Intent(getApplicationContext(), MainActivity.class);
            user.edit().clear().commit();
            startActivity(main);
        }
    }

    public void ChatAction() {
        final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String member_id = user.getString("member_id", "");
        if (member_id.equals("")) {
            Intent login = new Intent(getApplicationContext(), Login.class);
            login.putExtra("MAIN", 1);
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        } else {
        }
        // finish();
    }

    public void MainAction() {
        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(mainActivity);
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        SharedPreferences users = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String memeber_no = users.getString("member_no", "");
        String remember_token = users.getString("remember_token", "");

        String receiver_no = listUser.get(position).getReceiver().toString();
        String user = listUser.get(position).getUserName().toString();
        Intent chat = new Intent(ListChatUser.this, ChatActivity.class);
        SharedPreferences chatSetting = getSharedPreferences("chatSetting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = chatSetting.edit();
        editor.putString("receiver_no", receiver_no);
        editor.putString("memeber_no", memeber_no);
        editor.putString("remember_token", remember_token);

        editor.commit();
        chat.putExtra("receiver_no", receiver_no);
        chat.putExtra("user", user);
        chat.putExtra("listchat",4);
        startActivity(chat);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnHome:
                MainAction();
                break;
            case R.id.btnchat:
                ChatAction();
                break;
            case R.id.btnlogin:
                ASettingAction();
                break;
            case R.id.btnPerson:
                loginAction();
                break;
            case R.id.btnreservedCar:
                final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                String member_id = user.getString("member_id", "");
                if (member_id.equals("")) {
                    Intent login = new Intent(getApplicationContext(), Login.class);
                    login.putExtra("ListCarBack", "2");
                    login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(login);
                } else {
                    Intent orderlist = new Intent(getApplicationContext(), Orderlist.class);
                    startActivity(orderlist);
                    finish();
                }
                break;
        }
    }

    // Defines a runnable which is run every 100ms
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            new getUserList().execute();
            // new getNewMsglistUser().execute();
            handler.postDelayed(this, 20000);

        }
    };
        private class getUserList extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... args) {
            UserFunctions userFunction = new UserFunctions();
            SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String memeber_no = user.getString("member_no", "");
            String remember_token = user.getString("remember_token", "");
            Log.e("member_no", "" + memeber_no);
            Log.e("remember_token", remember_token);
            JSONObject json = userFunction.UserChat(memeber_no, remember_token);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            /**
             * Checks for success message.
             **/
            try {

                String res = json.getString("success");
                Log.d("key suc: ", "> " + res);

                if (res.equals("1")) {
                    JSONArray jsonarray = json.getJSONArray("user_info");
                    listUser = new ArrayList<User>();
                    //list = new ArrayList<String>();
                    Log.d("leaght", String.valueOf(jsonarray.length()));
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);
                        //list.add(jsonobject.getString("member_first_name"));

                        User mUser = new User();
                        String revice = jsonobject.getString("member_no");
                        mUser.setReceiver(jsonobject.getString("member_no"));
                        mUser.setUserName(jsonobject.getString("member_first_name"));
                        mUser.setLastname(jsonobject.getString("member_last_name"));
                        String newmsgcount = jsonobject.getString("new_count");
                        mUser.setMsg(newmsgcount);
                        listUser.add(mUser);
                        adapter = new ListUserAdapter(ListChatUser.this, listUser);
                        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(ListChatUser.this, android.R.layout.simple_list_item_1, list );
                        listView = (ListView) findViewById(R.id.lvUser);
                        listView.setAdapter(adapter);
                        listView.setOnItemClickListener(ListChatUser.this);
                        Log.e("HellNew=",""+jsonobject.getString("new_count"));
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            handler.postDelayed(runnable, 40000);
            }
        }
    private String tag="Chat Activity";
    public void onStart()
    {
        super.onStart();
        Log.d(tag, "In the onStart() event");
    }
    public void onRestart()
    {
        super.onRestart();
        handler.postDelayed(runnable, 20000);
        Log.d(tag, "In the onRestart() event");
    }
    public void onResume()
    {
        super.onResume();
        Log.d(tag, "In the onResume() event");
    }
    public void onPause()
    {
        super.onPause();
        //this.finish();
        Log.d(tag, "In the onPause() event");
    }
    public void onStop()
    {
        super.onStop();
        handler.removeCallbacks(runnable);
        Log.d(tag, "In the onStop() event");
    }
    @Override
    protected void onDestroy() {

        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
    }

