package all_action.iblaudas;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("ResourceAsColor")
public class AcountSettin extends Activity implements OnClickListener {
	private Button btnReserved;
	private Button btnHome;
	private Button btnchat;
	private Button btnlogin;
	private ImageView lockperson;
    SharedPreferences user;
    String member_no;
	@TargetApi(Build.VERSION_CODES.HONEYCOMB) @SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	setContentView(R.layout.setting_account);
	ActionBar bar = getActionBar();
	bar.hide();
	FrameLayout header = (FrameLayout)findViewById(R.id.headerView);
	getLayoutInflater().inflate(R.layout.header_search, header);
	FrameLayout content = (FrameLayout)findViewById(R.id.contentHome);
	getLayoutInflater().inflate(R.layout.content_setting, content);
	
	 user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
	 member_no = user.getString("member_id", "");
	String fname = user.getString("member_first_name", "");
	String lanme = user.getString("member_last_name", "");
	String email = user.getString("email", "");
	String country = user.getString("member_country", "");
	
	
	TextView membID = (TextView)findViewById(R.id.txtId);
	membID.setText(member_no);
	
	TextView fullanme = (TextView)findViewById(R.id.txtcar_fullanme);
	fullanme.setText(fname+" "+lanme);
	TextView myemail = (TextView)findViewById(R.id.txtemail);
	myemail.setText(email);
	TextView mycpountry = (TextView)findViewById(R.id.coutyy);
	mycpountry.setText(country);
	lockperson = (ImageView)findViewById(R.id.btnPerson);

	
	FrameLayout footer = (FrameLayout)findViewById(R.id.FootView);
	getLayoutInflater().inflate(R.layout.footer_all, footer);
	
	btnHome = (Button)findViewById(R.id.btnHome);
	btnchat = (Button)findViewById(R.id.btnchat);
	btnlogin = (Button)findViewById(R.id.btnlogin);
	btnReserved = (Button)findViewById(R.id.btnreservedCar);
	btnHome.setOnClickListener(this);
	btnchat.setOnClickListener(this);
	btnlogin.setOnClickListener(this);
	lockperson.setOnClickListener(this);
	btnReserved.setOnClickListener(this);
	
	TextView logt = (TextView)findViewById(R.id.txt_log);
	logt.setTextColor(R.color.myblue_text);

	
	btnlogin.setBackgroundResource(R.drawable.login_person_selected);
	if(member_no.equals("")){
		logt.setText(R.string.setlogin);
	}else{
		logt.setText(R.string.txt_account);
		lockperson.setImageResource(R.drawable.logout);
		
		}
	}
	public void ASettingAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		String member_id = user.getString("member_id", "");
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent setting = new Intent(getApplicationContext(),AcountSettin.class);
			setting.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(setting);
			finish();	
		}
	}
	public void loginAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		
		String member_id = user.getString("member_id", "");

		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent main = new Intent(getApplicationContext(), MainActivity.class);
			user.edit().clear().commit();
			startActivity(main);
		}
	}
	public void ChatAction(){
        if(member_no.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", 1);
            //Toast.makeText(getApplicationContext(), main, 100).show();
            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        }else {
            Intent chatActivity = new Intent(this, ListChatUser.class);
            chatActivity.putExtra("MAIN", 1);
            startActivity(chatActivity);
            finish();
        }
	}
	public void MainAction(){
		Intent mainActivity = new Intent(this,MainActivity.class); 
		//mainActivity.putExtra("MAIN", main);
		startActivity(mainActivity);
		 finish();
	}
	public void ReservedAction(){
		Intent reserved = new Intent(this,Orderlist.class); 
		//mainActivity.putExtra("MAIN", main);
		startActivity(reserved);
	  finish();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnHome:
			MainAction();
				break;
		case R.id.btnchat:
			ChatAction();
			break;
		case R.id.btnlogin:
			//loginAction();
			//loginAction();
			break;
		case R.id.btnPerson:
			loginAction();
			
			break;

		case R.id.btnreservedCar:
			ReservedAction();
			break;
			}
	}
	
}
