package all_action.iblaudas;



import ibaud.library.UserFunctions;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import all_action.iblaudas.adater.ViewPagerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class DetailCar extends Activity implements OnClickListener{
	ListView listview;
	//AdapterListDatail adapter;
    //private static String ServerURL = "http://192.168.0.210";
    private static String ServerURL = "http://iblauda.com/";
	String detialback = "3";
	ProgressDialog mProgressDialog;
	ArrayList<HashMap<String, String>> DataArrayDetail;
	ArrayList<HashMap<String, String>> arraylistImage;

	static String TAG_MODEL = "car_model";
	static String TAG_NO = "car_stock_no";
	static String TAG_MAKE = "car_make";
	static String TAG_YEAR= "car_year";
	
	static String TAG_COUNTRY = "country";
	static String TAG_CAR_CLASSNO = "car_chassis_no";
	static String TAG_CAR_GRAND = "car_grade";
	
	static String TAG_TRANSITION = "car_transmission";
	static String TAG_MAILEAGE = "car_mileage";
	static String TAG_FUEL = "car_fuel";
	
	static String TAG_COLOR = "car_color";
	static String TAG_SEAT = "car_seat";
	static String TAG_BODY_TYPE = "car_body_type";
	static String TAG_DRIVE_TYPE = "car_drive_type";
	static String TAG_ICON_STATUS = "icon_status";
	static String TAG_FOB_COST = "car_fob_cost";

	
	static String TAG_CAR_IMAGE = "full_image_url";
	public static final String DEFAULT ="";
	
	private Button btnReserved;
	private Button btnHome;

	String member_no, remember_token;
	TextView log;
	
	
	TextView D_make_car;
	TextView D_model_car;
	TextView D_year_car;
	TextView D_stok_car;
	TextView D_year;
	TextView D_classno;
	TextView D_make;
	TextView D_model;
	TextView D_grand;
	TextView D_model_yeaar;
	TextView D_transmission;
	
	TextView D_petrol,mufunafact;
	TextView sizeEnginee;
	TextView txtcar_millegi;
	TextView D_color;
	TextView D_seat;
	TextView D_bodytype;
	TextView D_drive_tpye;
	TextView D_arrailble;
	TextView D_cusfob;
	ProgressBar spinner;
	String carNo,carmodelYear,addfob,fuel,sizeEngin, carmake,carmodel,caryear,carcounty,carclassno,cargrand,cartran,carmaileage,carcolor,carseat,carbodptye,cardivetpye,caricon;
    private SharedPreferences user;
    String member_id;
    @TargetApi(Build.VERSION_CODES.HONEYCOMB) @SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from listview_main.xml
		ActionBar bar = getActionBar();
		bar.hide();
		setContentView(R.layout.car_detail);
		
		FrameLayout header = (FrameLayout)findViewById(R.id.headerView);
		getLayoutInflater().inflate(R.layout.header_product_detail, header);
		
		// FrameLayout contentDetail = (FrameLayout)findViewById(R.id.contentView_detail);
		// getLayoutInflater().inflate(R.layout.detail_layout, contentDetail);
		 
		// FrameLayout listdetail = (FrameLayout)findViewById(R.id.listdetail);
		// getLayoutInflater().inflate(R.layout.detail_list, listdetail);
		 
		 ImageView btnLogin = (ImageView)findViewById(R.id.btnPerson);
		 ImageView btnback = (ImageView)findViewById(R.id.btnBack);
			
		 
		 
		 
		 
			FrameLayout footer = (FrameLayout)findViewById(R.id.FootView);
			getLayoutInflater().inflate(R.layout.footer, footer);
			
			FrameLayout footer_all = (FrameLayout)findViewById(R.id.FooterViewsub);
			getLayoutInflater().inflate(R.layout.footer_all, footer_all);
			
			
			//===================================================
			RelativeLayout footerclick = (RelativeLayout)findViewById(R.id.footerclick);
			footerclick.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
				//Toast.makeText(getApplicationContext(), "Footer", 100).show();	
				}
			});
			
			 btnback.setOnClickListener(this); 
				
				
			 log = (TextView)findViewById(R.id.txt_log);
			 Button chat = (Button)findViewById(R.id.btnchat);
			 Button Login1 = (Button)findViewById(R.id.btnlogin);
			 
			 Button btnReverse = (Button)findViewById(R.id.btnReverse);
			 btnReverse.setOnClickListener(this);
			 chat.setOnClickListener(this);
			 Login1.setOnClickListener(this);
			
			  user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
			  member_id = user.getString("member_id", DEFAULT);
				//TextView logt = (TextView)findViewById(R.id.txt_log);
				
				if(member_id.equals("")){
					log.setText("Log in");
				}else{
					btnLogin.setImageResource(R.drawable.logout);
					log.setText("Account");
					}
			
			
			btnReserved = (Button)findViewById(R.id.btnreservedCar);
			btnHome = (Button)findViewById(R.id.btnHome);
			btnHome.setOnClickListener(this);
			btnReserved.setOnClickListener(this);	
			
			btnLogin.setOnClickListener(this);
			 new ActivityDetial().execute();
	}
	
	private class ActivityDetial extends AsyncTask<Void, Void, Void>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(DetailCar.this);
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Intent getIND = getIntent();
			String getID = getIND.getStringExtra("IDX");
			String detailur = ServerURL+"/?c=json&m=getCarDetails&idx="+getID;
			String urlImage = ServerURL+"/?c=json&m=getCarImages&idx="+getID;
			
				try {
					/*GetAllowFromHttps  httpsClientsdetaillist = new GetAllowFromHttps();
					HttpClient httpClientDet = httpsClientsdetaillist.getNewHttpClient();

					FunctionGetReaderHttpsURL getjsondetail = new FunctionGetReaderHttpsURL();
					String listDetailCar = getjsondetail.HttpsReaderUrl(httpClientDet, detailur);
					JSONArray json_list = new JSONArray(listDetailCar);*/
                    JSON_buffer jsonDetail = new JSON_buffer();
                    JSONArray json_list = new JSONArray(jsonDetail.getJSONUrl(detailur));
					DataArrayDetail = new ArrayList<HashMap<String, String>>();
					//HashMap<String,String> listCarDetail;
					// looping through All Contacts
					int myNum = 0;	
					for (int i = 0; i < json_list.length(); i++) {
						JSONObject c = json_list.getJSONObject(i);
						//listCarDetail = new HashMap<String, String>();
						 carcounty = c.getString(TAG_COUNTRY);
						 carNo = c.getString(TAG_NO);
						 carclassno = c.getString(TAG_CAR_CLASSNO);
						 
						 carmake = c.getString(TAG_MAKE);
						 carmodel = c.getString(TAG_MODEL);
						 cargrand = c.getString(TAG_CAR_GRAND);
						 caryear = c.getString(TAG_YEAR);
						  
						 carmodelYear = c.getString("car_year_start");
						 
						 fuel = c.getString("car_fuel");
						 sizeEngin = c.getString("car_cc");
						 
						 cartran = c.getString(TAG_TRANSITION);
						 carmaileage = c.getString(TAG_MAILEAGE);
						 carcolor = c.getString(TAG_COLOR);
						 carseat = c.getString(TAG_SEAT);
						 carbodptye= c.getString(TAG_BODY_TYPE);
						 cardivetpye = c.getString(TAG_DRIVE_TYPE);
						 caricon = c.getString(TAG_ICON_STATUS);
						
						
						 addfob = c.getString(TAG_FOB_COST);
						
						if(addfob.equals("0")|| addfob.equals("null")){
							addfob = "ASK";
							//movie.setFOB("FOB: ASK");
						}else{
							
							myNum = Integer.parseInt(c.getString(TAG_FOB_COST).toString());
							DecimalFormat df = new DecimalFormat("#,###");
							String result = df.format(myNum);
							//movie.setFOB("FOB: "+result+" USD($)");;
							addfob = result+" USD($)";
						}

						 fuel = c.getString(TAG_FUEL);
						//DataArrayDetail.add(listCarDetail);
						//getParam = c.getString(TAG_ICON_STATUS);
						Log.e("DetailListCar", ""+DataArrayDetail);
					}
					JSON_buffer imageCar = new JSON_buffer();
					JSONArray json_image = new JSONArray(imageCar.getJSONUrl(urlImage));
					arraylistImage = new ArrayList<HashMap<String, String>>();
					HashMap<String,String> listImage;
					// looping through All Contacts
					for (int s = 0; s < json_image.length(); s++) {
						JSONObject d = json_image.getJSONObject(s);
						listImage = new HashMap<String, String>();
						listImage.put("idx", d.getString("idx"));
						listImage.put("full_image_url", d.getString("full_image_url"));
						arraylistImage.add(listImage);
						Log.e("Image", ""+arraylistImage);
					}	
					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
			
			return null;	
		}

		@Override
		protected void onPostExecute(Void args) {
		
			  ViewPager viewPager = (ViewPager) findViewById(R.id.pagerViewdsd);
		      // Pass results to ViewPagerAdapter Class
			  ViewPagerAdapter adaptera = new ViewPagerAdapter(DetailCar.this,arraylistImage);
			  viewPager.setAdapter(adaptera);
			  Resources res = getResources();
			  viewPager.getLayoutParams().height=res.getDimensionPixelSize(R.dimen.SizeImageSwip);
			  //viewPager.getLayoutParams()
			  
			    D_make_car = (TextView)findViewById(R.id.txt_make_car);
				D_make_car.setText(carmake);
				
				D_model_car = (TextView)findViewById(R.id.txt_model);
				D_model_car.setText(carmodel);
				
				D_year_car = (TextView)findViewById(R.id.txt_year);
				D_year_car.setText(caryear);
				
				D_stok_car = (TextView) findViewById(R.id.txtcar_stockCar);
				D_stok_car.setText("NO."+carNo);
				
				D_year = (TextView) findViewById(R.id.txtcar_location);
				D_year.setText(carcounty);
				
				D_classno = (TextView) findViewById(R.id.txtcar_classno);
				D_classno.setText(carclassno);
				
				D_make = (TextView) findViewById(R.id.txtcar_make);
				D_make.setText(carmake);
				
				D_model = (TextView) findViewById(R.id.txtcar_model);
				D_model.setText(carmodel);
				
				D_grand = (TextView) findViewById(R.id.txtcar_grand);
				D_grand.setText(cargrand);
				
				D_model_yeaar = (TextView) findViewById(R.id.txtcar_model_year);
				D_model_yeaar.setText(carmodelYear);
				
				mufunafact = (TextView) findViewById(R.id.txtcar_manufature);
				mufunafact.setText(caryear);
				
				D_transmission = (TextView) findViewById(R.id.txtcar_transmission);
				D_transmission.setText(cartran);
				
				D_petrol = (TextView) findViewById(R.id.txtcar_pretrol);
				D_petrol.setText(fuel);
				sizeEnginee = (TextView) findViewById(R.id.txtcar_engignzize);
				sizeEnginee.setText(sizeEngin);
				
				
				txtcar_millegi = (TextView) findViewById(R.id.txtcar_millegi);
				txtcar_millegi.setText(carmaileage);
				
				D_color = (TextView) findViewById(R.id.txtcar_color);
				D_color.setText(carcolor);
				
				D_seat = (TextView) findViewById(R.id.txtcar_seat);
				D_seat.setText(carseat);
				
				D_bodytype = (TextView) findViewById(R.id.txtcar_body_type);
				D_bodytype.setText(carbodptye);
				
				D_drive_tpye = (TextView) findViewById(R.id.txtcar_drive_type);
				D_drive_tpye.setText(cardivetpye);
				
				D_arrailble = (TextView) findViewById(R.id.txtcar_arriable);
				D_arrailble.setText(caricon);
				
				D_cusfob = (TextView) findViewById(R.id.txtcar_fob);
				D_cusfob.setText(addfob);

			  Button btnChat = (Button)findViewById(R.id.btnChat);
			  btnChat.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
                    if(member_id.equals("")){
                        Intent login = new Intent(getApplicationContext(),Login.class);
                        login.putExtra("MAIN", 1);
                        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                        finish();
                    }else {
                        Intent chat = new Intent(DetailCar.this, ListChatUser.class);
                        Intent getIND = getIntent();
                        String getID = getIND.getStringExtra("IDX");
                        chat.putExtra("IDX", getID);
                        chat.putExtra("cardetail", detialback);
                        chat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(chat);
                        //finish();
                    }
				}
			});
			mProgressDialog.dismiss();
		}
	
	}
	public void ASettingAction(){
		 user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
	     member_id = user.getString("member_id", DEFAULT);
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent setting = new Intent(getApplicationContext(),AcountSettin.class);
			setting.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(setting);
			finish();	
		}
	} 
	public void loginAction(){
		final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		String member_id = user.getString("member_id", "");
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent main = new Intent(getApplicationContext(), MainActivity.class);
			user.edit().clear().commit();
			startActivity(main);
		}
	}
	public void ChatAction(){
        if(member_id.equals("")){
            Intent login = new Intent(getApplicationContext(),Login.class);
            login.putExtra("MAIN", 1);

            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
            finish();
        }else {
            Intent chat = new Intent(getApplicationContext(), ListChatUser.class);
            chat.putExtra("MAIN", 1);
            Intent getIND = getIntent();
            String getID = getIND.getStringExtra("IDX");
            chat.putExtra("IDX", getID);
            chat.putExtra("cardetail", detialback);
            startActivity(chat);
           // finish();
        }
	}
	public void MainAction(){
		Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class); 
		startActivity(mainActivity);
		 finish();
	}
	public void ReservedAction(){
		 user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
		 member_id = user.getString("member_id", "");
		if(member_id.equals("")){
			Intent login = new Intent(getApplicationContext(),Login.class);
			login.putExtra("MAIN", 1);
			//Toast.makeText(getApplicationContext(), main, 100).show();
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(login);
			finish();	
		}else{
			Intent reserved = new Intent(this,Orderlist.class); 
			//mainActivity.putExtra("MAIN", main);
			startActivity(reserved);
		  finish();
		}
		
	}
	@SuppressLint("ShowToast") @Override
	public void onClick(View v) {
		
		Intent getIND = getIntent();
		String getpbackcarlist = getIND.getStringExtra("url_list");
		String value_backMain = getIND.getStringExtra("MAIN");
		String value_listCar = getIND.getStringExtra("ListCarBack");
		String getID = getIND.getStringExtra("IDX");
		 //user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
      //  member_id = user.getString("member_id", DEFAULT);
		
		switch(v.getId()){
		case R.id.btnHome:
			MainAction();
			break;
		case R.id.btnchat:
			ChatAction();
			break;
				
		case R.id.btnBack:
			if(value_backMain!=null){
				Intent backMain = new Intent(this,MainActivity.class);
				startActivity(backMain);
				finish();
			}else if(value_listCar!=null){
			
				Intent back = new Intent(this,CarList.class);
				back.putExtra("url_list", getpbackcarlist);
				startActivity(back);
				finish();
			}else{
				Intent backMain = new Intent(this,MainActivity.class);
				startActivity(backMain);
				finish();
			}
			
		break;
		case R.id.btnReverse:
			if (!member_id.equals("")){
				new NetCheck1().execute();
			}else{
				Intent login = new Intent (DetailCar.this,Login.class);
				login.putExtra("cardetail", detialback);
				login.putExtra("IDX", getID);
				startActivity(login);
				finish();
			}
			break;
			
		case R.id.btnlogin:
			ASettingAction();
			break;
		case R.id.btnPerson:
			loginAction();
			break;
		case R.id.btnreservedCar:
			if (!member_id.equals("")){
				Intent orderlist = new Intent (DetailCar.this,Orderlist.class);
				orderlist.putExtra("cardetail", detialback);
				orderlist.putExtra("IDX", getID);
				startActivity(orderlist);
				finish();
			
			}else{
				Intent login = new Intent (DetailCar.this,Login.class);
				login.putExtra("cardetail", detialback);
				login.putExtra("IDX", getID);
				startActivity(login);
				finish();
			}
			break;
			}
	}
	
	
	/**
     * Async Task to check whether internet connection is working
     **/

    private class NetCheck1 extends AsyncTask<String,String,Boolean>
    {
        private ProgressDialog nDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            nDialog = new ProgressDialog(DetailCar.this);
            nDialog.setMessage("Loading..");
            nDialog.setTitle("Checking Internate");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(true);
            nDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... args){
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(Boolean th){

            if(th == true){
                nDialog.dismiss();
                new ProcessOrderPOST().execute();
            }
            else{
                nDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Error network connection", Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    private class ProcessOrderPOST extends AsyncTask<String, String, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
    		member_no = user.getString("member_no", DEFAULT);
    		remember_token = user.getString("remember_token", DEFAULT);
    		 
            
        }

        @Override
        protected JSONObject doInBackground(String... args) {

        	Intent getIND = getIntent();
    		String getID = getIND.getStringExtra("IDX");
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.ReVersCar(member_no, remember_token,getID );

            return json;


        }
        
        @Override
        protected void onPostExecute(JSONObject json) {
        	String result;
        	String sucessMsg;
			try {
				result = json.getString("success");
				//msg_error = json.getString("error_msg");
				sucessMsg = json.getString("msg");
				Log.e("Json_DAT", ""+result);
				Log.e("Json_DAT", ""+sucessMsg);
                
				if(result.equals("1")){
					Log.e("JsonRe1", ""+result);
					Log.e("Json_re1Er", ""+sucessMsg);
					//pDialog.dismiss();
					Toast.makeText(getApplicationContext(), sucessMsg, Toast.LENGTH_LONG).show();
				}else if(result.equals("0")){
					Log.e("JsonRe0", ""+result);
					Log.e("Json_re0Er", ""+sucessMsg);
					//pDialog.dismiss();
					Toast.makeText(getApplicationContext(), sucessMsg, Toast.LENGTH_LONG).show();
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
           
			
        }
        
    }
   
       
  	
}
