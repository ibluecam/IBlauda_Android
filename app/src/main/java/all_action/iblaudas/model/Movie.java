package all_action.iblaudas.model;



public class Movie {
    private String index,title,country,note,carCity,Fob, thumbnailUrl,
            status_icon,
            create_status;
    private boolean checked = false;

    public Movie() {
    }

    public Movie(String index,String note,String name, String thumbnailUrl, String Fob, String country,
                 String carCity,String status_icon,
                 String create_status) {
        this.index = index;
        this.note = note;
        this.title = name;
        this.thumbnailUrl = thumbnailUrl;
        this.Fob = Fob;
        this.country = country;
        this.carCity = carCity;
        this.status_icon = status_icon;
        this.create_status = create_status;
    }

    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }
    public String getTitle() {
        return title;
    }

    public String getNOT() {
        return note;
    }
    public void setnote(String note) {
        this.note = note;
    }


    public void setTitle(String name) {
        this.title = name;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getFOB() {
        return Fob;
    }

    public void setFOB(String Fob) {
        this.Fob = Fob;
    }

    public String getCountry() {
        return country;
    }

    public void setContry(String country) {
        this.country = country;
    }

    public String getCarCity() {
        return carCity;
    }

    public void setCarCity(String carCity) {
        this.carCity = carCity;
    }

    public void setStatusIcon(String status_icon) {
        this.status_icon = status_icon;
    }
    public String getStatusIcon(){return status_icon;}
    public void setCreateStatus(String create_status){this.create_status = create_status;}
    public String getCreate_status(){return create_status;}

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
