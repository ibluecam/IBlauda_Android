package all_action.iblaudas;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gcm.GCMRegistrar;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import TaskModelAndAdapter.Task;
import all_action.iblaudas.adater.CustomListAdapter;
import all_action.iblaudas.app.AppController;
import all_action.iblaudas.model.Movie;
import ibaud.library.getCC;

import static all_action.iblaudas.CommonUtilities.SENDER_ID;



@SuppressLint({ "NewApi", "ShowToast" }) 
public class MainActivity extends Activity implements OnClickListener {
    String codename,codenames ,Fullname,countryCodeName;
    ListView listview;
    //ListViewAdapter adapter;

    String make;

    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    ArrayList<HashMap<String, String>> arraylistCountry;
    List<String> listist;
    ArrayList<HashMap<String, String>> arraylistMak;
    List<String> listmake;
    ArrayList<HashMap<String, String>> arraylistModel;
    List<String> listmodel;
    static String TAG_IDEX = "idx";
    static String TAG_CAR_NO = "car_stock_no";
    static String TAG_YEAR = "car_year";
    static String TAG_MAKE = "car_make";
    static String TAG_COUNTRY = "country";
    static String TAG_FOB_COST = "car_fob_cost";
    static String TAG_CAR_CITY = "car_city";
    static String TAG_FOB_CURRENT = "car_fob_currency";
    static String TAG_NULL = " ";
    static String TAG_IMAGE = "image_name";
    static String TAG_ICON_STATUS = "icon_status";
    static String TAG_CREATE_STATUS = "created_status";
    static String TAG_MODEL = "car_model";
    //private static String ServerURL = "http://192.168.0.210";
    private static String ServerURL = "http://iblauda.com";
    private String url = ServerURL + "/?c=json&m=getCarListpage";

    private String url_getCountry = ServerURL + "/?c=json&m=getCarCountryList";

    private String make_url = ServerURL + "/?c=json&m=getMake";

    private String model_url = ServerURL + "/?c=json&m=getModel&car_make=TOYOTA";

    private Button btnSearch;

    String main = "1";
    String carlistback = "2";
    String detialback = "3";

    //===========var get country=============
    static String TAG_CID = "id";
    static String TAG_CAR_COUNTRY = "car_country";
    static String TAG_CNUMBER_CODE = "numcode";
    static String TAG_CPHONE_CODE = "phonecode";

    static String TAG_CAR_MAKE = "car_make";

    String location;
    Spinner spinner_country;
    Spinner spinner_make;
    Spinner spinner_model;
    EditText spinner;
    EditText nin_year;
    EditText nax_year;
    EditText nan_price;

    String getCountry;
    String makelist;
    String eModel;
    String eminprice;
    String emaxpric;
    String eninyear;
    String emaxyea;
    private List<Movie> movieList = new ArrayList<Movie>();
    private ListView listView;
    private CustomListAdapter adapter;
    ScrollView scrollView;
    ArrayList<getCC> listCC;
    ArrayList<String> worldlist;

    public static final String DEFAULT = "";
    private Button btnReserved;
    private Button btnHome;
    private Button btnchat;
    private Button btnlogin;

    public static final String YOUR_APPLICATION_ID = "rZpFfTt5XYaB4ZJv1DAYDbq2OEaCyAD2ZF3aPnRv";
    public static final String YOUR_CLIENT_KEY = "SICSqFZPlyctZk4oacZVQCRZnA2ClG3xbcfKWw0i";
    TelephonyManager tm;
    // label to display gcm messages
    TextView lblMessage;

    // Asyntask
    AsyncTask<Void, Void, Void> mRegisterTask;

    // Alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();

    // Connection detector
    ConnectionDetector cd;

    // String member_id;
    //SharedPreferences user;
    @SuppressLint({"NewApi", "CutPasteId", "ResourceAsColor"})
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        ActionBar bar = getActionBar();
        bar.hide();
        cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(MainActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return

        } else {
            Parse.initialize(this, YOUR_APPLICATION_ID, YOUR_CLIENT_KEY);
            ParseAnalytics.trackAppOpened(getIntent());
            ParseObject.registerSubclass(Task.class);

            Resources res = getResources();
            setContentView(R.layout.search_carlist);
            scrollView = (ScrollView) findViewById(R.id.scrollView1);
            FrameLayout header = (FrameLayout) findViewById(R.id.headerView);
            getLayoutInflater().inflate(R.layout.header_search, header);

            FrameLayout content = (FrameLayout) findViewById(R.id.content_Search_Car);
            getLayoutInflater().inflate(R.layout.content_carsearch, content);


            //==========notification========


            LinearLayout listdata = (LinearLayout) findViewById(R.id.listscroll);
            listdata.getLayoutParams().height = res.getDimensionPixelSize(R.dimen.listhome);

            FrameLayout footer = (FrameLayout) findViewById(R.id.FootView);
            getLayoutInflater().inflate(R.layout.footer_all, footer);
            final ImageView btnLogin = (ImageView) findViewById(R.id.btnPerson);

            //==============================================
            btnSearch = (Button) findViewById(R.id.btn_search);

            btnHome = (Button) findViewById(R.id.btnHome);
            btnHome.setBackgroundResource(R.drawable.icon_home_selected);
            TextView home = (TextView) findViewById(R.id.txt_home);
            home.setTextColor(R.color.myblue_text);
            btnchat = (Button) findViewById(R.id.btnchat);
            btnlogin = (Button) findViewById(R.id.btnlogin);
            btnReserved = (Button) findViewById(R.id.btnreservedCar);
            //===================================================
            RelativeLayout footerclick = (RelativeLayout) findViewById(R.id.footerclick);
            footerclick.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    //Toast.makeText(getApplicationContext(), "Footer", 100).show();
                }
            });

            btnLogin.setOnClickListener(this);
            btnHome.setOnClickListener(this);
            btnchat.setOnClickListener(this);
            btnlogin.setOnClickListener(this);
            btnReserved.setOnClickListener(this);

            SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String member_id = user.getString("member_id", DEFAULT);
            TextView logt = (TextView) findViewById(R.id.txt_log);
            if (member_id.equals("")) {
                logt.setText(R.string.setlogin);
            } else {
                btnLogin.setImageResource(R.drawable.logout);
                logt.setText(R.string.txt_account);
            }


            btnSearch.setOnClickListener(this);
            //new JSONObject();
            listView = (ListView) findViewById(R.id.listview_featureCar);
            adapter = new CustomListAdapter(this, movieList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    String index_key = movieList.get(position).getIndex().toString();
                    String countryget = movieList.get(position).getCountry().toString();
                    Intent i = new Intent(getApplicationContext(), DetailCar.class);
                    i.putExtra("IDX", index_key);
                    i.putExtra(TAG_COUNTRY, countryget);
                    i.putExtra("MAIN", main);
                    // Toast.makeText(getApplicationContext(), index_key, 100).show();
                    startActivity(i);
                    finish();

                }
            });
            JsonArrayRequest movieReq = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            //Log.d(TAG, response.toString());
                            int myNum = 0;            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    Movie movie = new Movie();
                                    movie.setIndex(obj.getString(TAG_IDEX));
                                    movie.setnote("NO: " + obj.getString(TAG_CAR_NO));
                                    movie.setTitle(obj.getString(TAG_MAKE) + " " + obj.getString(TAG_MODEL) + " " + obj.getString(TAG_YEAR));
                                    movie.setContry(obj.getString(TAG_COUNTRY));
                                    movie.setCarCity(obj.getString(TAG_CAR_CITY));
                                    movie.setStatusIcon(obj.getString(TAG_ICON_STATUS));
                                    movie.setCreateStatus(obj.getString(TAG_CREATE_STATUS));
                                    String addfob = obj.getString(TAG_FOB_COST);
                                    if (addfob.equals("0") || addfob.equals("null")) {
                                        movie.setFOB("FOB: ASK");
                                    } else {
                                        myNum = Integer.parseInt(obj.getString(TAG_FOB_COST).toString());
                                        DecimalFormat df = new DecimalFormat("#,###");
                                        String result = df.format(myNum);
                                        movie.setFOB("FOB: " + result + " USD($)");
                                    }
                                    movie.setThumbnailUrl(obj.getString(TAG_IMAGE));
                                    Log.e("Url",obj.getString(TAG_IMAGE));
                                    movieList.add(movie);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            adapter.notifyDataSetChanged();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Json can not respone", "Error: " + error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(movieReq);

            new DownloadJSON().execute();
            new getModel().execute();
            GCMRegistrar.checkDevice(this);
            GCMRegistrar.checkManifest(this);
            final String regId = GCMRegistrar.getRegistrationId(this);

            // Check if regid already presents
            if (regId.equals("")) {
                // Registration is not present, register now with GCM
                GCMRegistrar.register(this, SENDER_ID);
            } else {
                // Device is already registered on GCM
                if (GCMRegistrar.isRegisteredOnServer(this)) {
                    //Toast.makeText(getApplicationContext(), regId, Toast.LENGTH_LONG).show();
                    // Skips registration.
                    //Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
                } else {
                    // Try to register again, but not in the UI thread.
                    // It's also necessary to cancel the thread onDestroy(),
                    // hence the use of AsyncTask instead of a raw thread.
                    final Context context = this;
                    mRegisterTask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            // Register on our server
                            // On server creates a new user
                            ServerUtilities.register(context, regId);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            mRegisterTask = null;
                        }

                    };
                    mRegisterTask.execute(null, null, null);
                }
            }
            Log.d("NumberPhoneID", "" + regId);
        }
        //==========notification========

    }


    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                JSON_buffer data_country = new JSON_buffer();
                JSONArray countryDATa = new JSONArray(data_country.getJSONUrl(url_getCountry));

                arraylistCountry = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> listCounty;
                listist = new ArrayList<String>();
                listist.add("Location");
                for (int i = 0; i < countryDATa.length(); i++) {
                    JSONObject c = countryDATa.getJSONObject(i);
                    listCounty = new HashMap<String, String>();

                    listCounty.put(TAG_CAR_COUNTRY, c.getString(TAG_CAR_COUNTRY));

                    arraylistCountry.add(listCounty);
                    String CarCountry = c.getString(TAG_CAR_COUNTRY);
                    if (CarCountry.equals("null") || CarCountry.equals(" ")) {
                        listist.remove(CarCountry);
                    } else {
                        listist.add(CarCountry);
                    }

                }

                JSON_buffer data_make = new JSON_buffer();
                JSONArray makDATa = new JSONArray(data_make.getJSONUrl(make_url));
                arraylistMak = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> listMake;
                listmake = new ArrayList<String>();
                listmake.add("Make");
                for (int i = 0; i < makDATa.length(); i++) {
                    JSONObject c = makDATa.getJSONObject(i);
                    listMake = new HashMap<String, String>();

                    listMake.put(TAG_CAR_MAKE, c.getString(TAG_CAR_MAKE));

                    arraylistMak.add(listMake);
                    String CarMakey = c.getString(TAG_CAR_MAKE);
                    if (CarMakey.equals("null") || CarMakey.equals(" ")) {
                        listmake.remove(CarMakey);
                    } else {
                        //Log.e("counry=:", ""+listCounty);
                        listmake.add(CarMakey);
                    }
                }


                String countryCodeName;
                String urlss = "http://iblauda.com/?c=json&m=getCountry";
                worldlist = new ArrayList<String>();
                listCC = new ArrayList<getCC>();
                    JSON_buffer data_countrys = new JSON_buffer();
                    //JSONArray json_model = new JSONArray();
                    JSONArray jsonarray = new JSONArray(data_countrys.getJSONUrl(urlss));
                    countryCodeName = tm.getNetworkCountryIso();
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(i);

                        getCC listAllCC = new getCC();
                        listAllCC.setCc(jsonobject.optString("cc"));
                        codename = jsonobject.optString("cc");

                        if (codename.equals(countryCodeName)) {
                            Fullname = jsonobject.getString("country_name");
                            codenames = jsonobject.optString("cc");
                            Log.e("Fullname=", jsonobject.optString("country_name"));
                            final SharedPreferences mylocation = getSharedPreferences("locaction", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = mylocation.edit();
                            editor.putString("countryname", Fullname);
                            editor.putString("countrycode", codenames);
                            editor.putString("countrycodeNumber", jsonobject.optString("phonecode"));
                            editor.commit();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;

            }

            @Override
            protected void onPostExecute (Void args){
                createSpinnerDropDown();
                //mProgressDialog.dismiss();
            }

        }

        private String selected_country = null;
        private String selected_make = null;
        private String selected_model = null;
        private String selected_nin_price;
        private String selected_nan_price;
        private String selected_nin_year;
        private String selected_nan_year;

        private void createSpinnerDropDown() {
            //get reference to the spinner from the XML layout
            Spinner spinner_country = (Spinner) findViewById(R.id.sp_location);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, listist);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_country.setAdapter(dataAdapter);

            spinner_make = (Spinner) findViewById(R.id.sp_make);
            // Spinner  modelsp = (Spinner) findViewById(R.id.sp_model);

            ArrayAdapter<String> dataAdapterMake = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, listmake);
            dataAdapterMake.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_make.setAdapter(dataAdapterMake);

            spinner = (EditText) findViewById(R.id.sp_nin_price);
            nan_price = (EditText) findViewById(R.id.sp_nan_price);
            nin_year = (EditText) findViewById(R.id.sp_nin_year);
            nax_year = (EditText) findViewById(R.id.sp_nan_year);
            selected_nin_price = spinner.getText().toString();
            selected_nan_price = nan_price.getText().toString();
            selected_nin_year = nin_year.getText().toString();
            selected_nan_year = nax_year.getText().toString();

            spinner_country.setOnItemSelectedListener(new MyOnItemSelectedListener());
            //spinner_country.setOnItemSelectedListener(this);
            spinner_make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub
                    String result = spinner_make.getSelectedItem().toString();
                    try {
                        String makedata = URLEncoder.encode(result, "UTF-8");

                        if (result != "Make") {
                            selected_make = makedata;

                        } else {
                            selected_make = "";
                        }

                        model_url = ServerURL + "/?c=json&m=getModel&car_make=" + selected_make;
                        new getModel().execute();
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });
        }

        public class MyOnItemSelectedListener implements OnItemSelectedListener {

            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                String selectedItem = parent.getItemAtPosition(pos).toString();

                //check which spinner triggered the listener
                switch (parent.getId()) {
                    //country spinner
                    case R.id.sp_location:
                        //make sure the country was already selected during the onCreate
                        if (selectedItem != "Location") {
                            selected_country = selectedItem;
                            // Toast.makeText(getApplicationContext(), selectedItem, 100).show();
                        } else {
                            selected_country = "";
                            // Toast.makeText(getApplicationContext(), selected_country, 100).show();
                        }
                        break;
//

                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing.
            }
        }

        public void ASettingAction() {
            final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String member_id = user.getString("member_id", DEFAULT);
            if (member_id.equals("")) {
                Intent login = new Intent(MainActivity.this, Login.class);
                login.putExtra("MAIN", main);
                //Toast.makeText(getApplicationContext(), main, 100).show();
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                finish();
            } else {
                Intent setting = new Intent(MainActivity.this, AcountSettin.class);
                setting.putExtra("MAIN", main);
                //Toast.makeText(getApplicationContext(), main, 100).show();
                setting.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(setting);
                finish();
            }
        }

        public void loginAction() {
            final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String member_id = user.getString("member_id", DEFAULT);
            if (member_id.equals("")) {
                Intent login = new Intent(MainActivity.this, Login.class);
                login.putExtra("MAIN", main);
                //Toast.makeText(getApplicationContext(), main, 100).show();
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                finish();
            } else {
                Intent main = new Intent(MainActivity.this, MainActivity.class);
                user.edit().clear().commit();
                startActivity(main);
                finish();
            }
        }

        public void ChatAction() {
            final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String member_id = user.getString("member_id", DEFAULT);
            if (member_id.equals("")) {
                Intent login = new Intent(MainActivity.this, Login.class);
                login.putExtra("MAIN", main);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(login);
                finish();
            } else {
                Intent chatActivity = new Intent(this, ListChatUser.class);

                chatActivity.putExtra("MAIN", main);
                startActivity(chatActivity);
                finish();
            }
        }

        public void MainAction() {
            Intent mainActivity = new Intent(this, MainActivity.class);
            //mainActivity.putExtra("MAIN", main);
            startActivity(mainActivity);
            finish();
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.btn_search:
                    String TAG_URL = "url_list";
                    try {
                        EditText txt_searchchassno;
                        Intent listCarURL = new Intent(this, CarList.class);
                        txt_searchchassno = (EditText) findViewById(R.id.offerNo_txt);
                        String chass_notxt = txt_searchchassno.getText().toString();
                        String getClassNo = URLEncoder.encode(chass_notxt, "UTF-8");
                        if (chass_notxt.equals("")) {
                            getCountry = URLEncoder.encode(selected_country, "UTF-8");
                            makelist = URLEncoder.encode(selected_make, "UTF-8");
                            eModel = URLEncoder.encode(selected_model, "UTF-8");
                            eminprice = URLEncoder.encode(selected_nin_price, "UTF-8");
                            emaxpric = URLEncoder.encode(selected_nan_price, "UTF-8");
                            eninyear = URLEncoder.encode(selected_nin_year, "UTF-8");
                            emaxyea = URLEncoder.encode(selected_nan_year, "UTF-8");

                            String sear_url = ServerURL + "/?c=json&m=getCarListpage&country=" + getCountry + "&car_make=" + makelist + "&car_model=" + eModel + "&pr_min=" + eminprice + "&pr_max=" + emaxpric + "&year_min=" + eninyear + "&year_max=" + emaxyea;
                            listCarURL.putExtra(TAG_URL, sear_url);
                            // Toast.makeText(getApplicationContext(), sear_url, 10000).show();
                        } else {
                            String cassnoURL = ServerURL + "/?c=json&m=getCarListpage&car_chassis_no=" + getClassNo;
                            listCarURL.putExtra(TAG_URL, cassnoURL);
                        }

                        startActivity(listCarURL);
                        finish();
                        break;
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                case R.id.btnHome:
                    //	MainAction();
                    break;
                case R.id.btnchat:
                    ChatAction();
                    break;
                case R.id.btnlogin:
                    //loginAction();
                    ASettingAction();
                    break;
                case R.id.btnPerson:
                    loginAction();

                    break;
                case R.id.btnreservedCar:
                    final SharedPreferences user = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                    String member_id = user.getString("member_id", DEFAULT);
                    if (member_id.equals("")) {
                        Intent login = new Intent(MainActivity.this, Login.class);
                        login.putExtra("MAIN", main);
                        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                        finish();
                    } else {
                        Intent i = new Intent(getApplicationContext(), Orderlist.class);
                        startActivity(i);
                        //finish();
                    }

                    break;

            }
        }

        private String tag = "lifecicle_Main";

        public void onStart() {
            super.onStart();
            Log.d(tag, "In the onStart() event");
        }

        public void onRestart() {
            super.onRestart();
            Log.d(tag, "In the onRestart() event");
        }

        public void onResume() {
            super.onResume();
            Log.d(tag, "In the onResume() event");
        }

        public void onPause() {
            super.onPause();
            Log.d(tag, "In the onPause() event");
        }

        public void onStop() {
            super.onStop();

            Log.d(tag, "In the onStop() event");
        }


        private class getModel extends AsyncTask<Void, Void, Void> {


            @Override
            protected Void doInBackground(Void... params) {

                try {

                    JSON_buffer data_model = new JSON_buffer();
                    JSONArray json_model = new JSONArray(data_model.getJSONUrl(model_url));
                    arraylistModel = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> listModel;
                    listmodel = new ArrayList<String>();
                    listmodel.add("Model");
                    for (int i = 0; i < json_model.length(); i++) {
                        JSONObject c = json_model.getJSONObject(i);
                        listModel = new HashMap<String, String>();
                        listModel.put(TAG_MODEL, c.getString(TAG_MODEL));
                        arraylistModel.add(listModel);
                        String CarModelKey = c.getString(TAG_MODEL);
                        if (CarModelKey.equals("null") || CarModelKey.equals(" ")) {
                            listmodel.remove(CarModelKey);
                        } else {
                            //Log.e("counry=:", ""+listCounty);
                            listmodel.add(CarModelKey);
                        }
                        Log.e("country_Name=:", "" + listmodel);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(Void args) {
                final Spinner spinner_model = (Spinner) findViewById(R.id.sp_model);

                ArrayAdapter<String> dataAdapterModel = new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.simple_spinner_item, listmodel);
                dataAdapterModel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_model.setAdapter(dataAdapterModel);
                spinner_model.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1,
                                               int arg2, long arg3) {
                        String modelsp = spinner_model.getSelectedItem().toString();
                        if (modelsp != "Model") {
                            selected_model = modelsp;
                        } else {
                            selected_model = "";
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }
                });
            }
        }


        /**
         * Receiving push messages
         *
         private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
        String newMessage = intent.getExtras().getString("");
        // Waking up mobile if it is sleeping
        WakeLocker.acquire(getApplicationContext());

        /*
         * Take appropriate action on this message
         * depending upon your app requirement
         * For now i am just displaying it on the screen


        // Showing received message
        lblMessage.append(newMessage + "\n");
        Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

        // Releasing wake lock
        WakeLocker.release();
        }
        };

         @Override protected void onDestroy() {

         try {
         unregisterReceiver(mHandleMessageReceiver);
         GCMRegistrar.onDestroy(this);
         } catch (Exception e) {
         Log.e("UnRegister Receiver Error", "> " + e.getMessage());
         }
         super.onDestroy();
         }
         */
    }

