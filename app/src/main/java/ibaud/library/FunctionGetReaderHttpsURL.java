package ibaud.library;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class FunctionGetReaderHttpsURL {
	
	public String HttpsReaderUrl(HttpClient httpClient,String url) {
		StringBuilder sb = new StringBuilder();
		try {
			HttpGet getRequest = new HttpGet(url);
			getRequest.addHeader("accept", "application/json");

			HttpResponse response = httpClient.execute(getRequest);

			String output;
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		httpClient.getConnectionManager().shutdown();
		return sb.toString();
		
	}
}
